package net.floodlightcontroller.util;

/** 
 * The class representing IPv4 address
 * @author Liu Wenxue
 */

public class IPv4Address {
    public static final int IP_ADDRESS_LENGTH = 4;
    private int address;

    public IPv4Address(int address){
        if (isVaild(address)){
            this.address = address;
        } else {
            throw new IllegalArgumentException("IPv4 address with Integer ABCABCABCABC " +
                    " where ABC must be 0 <= value <= 255");
        }
    }

    /**
     * Returns {@code true} if the address is the vaild IPv4 address.
     * @return {@code true} if the address is the vaild IPv4 address.
     */
    protected static boolean isVaild(int address){
        int octets = 0;
        for (int i = 0; i < IP_ADDRESS_LENGTH; ++i){
           octets = (address >> (8*(3-i))) & 0xff;
           if (octets > 255 || octets < 0){
               return false;
           }
        }
        return true;
    }

    /**
     * Returns a IPv4 Address instance representing the value of the specified {@code String}
     * @param address  the String representation of the IPv4 Address to be parsed.
     * @return a IPv4 Address instance representing the value of the specified {@code String}
     * @throws IllegalArgumentException if the String cannot be parsed as a IPv4 address
     */
    public static IPv4Address valueOf(String address){
        if (address == null){
            throw new IllegalArgumentException("String of IPv4 address cannot be null");
        }
        String [] octets = address.split("\\.");
        if (octets.length != IP_ADDRESS_LENGTH) {
            throw new IllegalArgumentException("Specified IPv4 address must" +
                "contain 4 sets of numerical digits separated by periods");
        }
        int result = 0;
        for (int i = 0; i < IP_ADDRESS_LENGTH; ++i){
            int oct = Integer.valueOf(octets[i]);
            if (oct >= 0 && oct <= 255) {
                result |= oct << (8*(3-i));
            } else {
                throw new IllegalArgumentException("Octet values in specified" +
                        " IPv4 address must be 0 <= value <= 255");
            }
        }
        return new IPv4Address(result);
    }

    /**
     * Returns a IPv4 Address instance representing the value of the specified {@code byte}
     * @param address  the byte array representation of the IPv4 Address to be parsed.
     * @return a IPv4 Address instance representing the value of the specified {@code byte}
     * @throws IllegalArgumentException if the byte array length isn't equal to 4.
     */
    public static IPv4Address valueOf(byte[] address){
        if (address == null){
            throw new IllegalArgumentException("the byte array of IPv4 address cannot be null");
        }
        if (address.length != IP_ADDRESS_LENGTH){
            throw new IllegalArgumentException("Specified IPv4 address must contain 4 byte array");
        }
        int result = 0;
        for (int i = 0; i < IP_ADDRESS_LENGTH; i++){
            result |= (address[i] & 0xff) << (8*(3-i));
        }
        return new IPv4Address(result);
    }

    /**
     * Returns a IPv4 Address instance representing the value of the specified {@code long}
     * @param address  the long representation of the IPv4 Address to be parsed.
     * @return a IPv4 Address instance representing the value of the specified {@code long}
     * @throws IllegalArgumentException if the long cannot be parsed as a IPv4 address
     */
    public static IPv4Address valueOf(long address){
        if (IPv4Address.isVaild((int)address)){
            return new IPv4Address((int)address);
        } else {
            throw new IllegalArgumentException("IPv4 address with Integer ABCABCABCABC " +
                    " where ABC must be 0 <= value <= 255");
        }
    }


    /**
     * Returns the value of the {@code IPv4Address} as a {@code byte} array.
     * @return a {@code byte} array representing the IPv4 address.
     */
    public byte[] toBytes(){
        return new byte[] {
                (byte)(this.address >>> 24),
                (byte)(this.address >>> 16),
                (byte)(this.address >>> 8),
                (byte)this.address};
        
    }

    /**
     * Returns the value of the {@code IPv4Address} as a {@code long}.
     * @return a numerical value representing the IPv4 address.
     */
    public long toLong(){
        return (long)this.address;
    }

    @Override
    public String toString(){
        StringBuffer sip = new StringBuffer();
        int result = 0;
        for (int i = 0; i < IP_ADDRESS_LENGTH; ++i){
            result = (this.address >> (8*(3-i))) & 0xff;
            sip.append(Integer.valueOf(result).toString());
            if (i != 3){
                sip.append(".");
            }
        }
        return sip.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof IPv4Address)) {
            return false;
        }

        IPv4Address other = (IPv4Address)o;
        return this.address == other.address;
    }

    @Override
    public int hashCode(){
        return Integer.valueOf(this.address).hashCode();
    }
}

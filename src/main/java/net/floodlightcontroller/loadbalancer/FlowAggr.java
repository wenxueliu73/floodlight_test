package net.floodlightcontroller.loadbalancer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.openflow.protocol.OFFlowMod;
import org.openflow.protocol.OFMatch;
import org.openflow.protocol.OFPacketOut;
import org.openflow.protocol.OFPort;
import org.openflow.protocol.OFType;
import org.openflow.protocol.Wildcards;
import org.openflow.protocol.action.OFAction;
import org.openflow.util.U16;

import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.counter.ICounterStoreService;
import net.floodlightcontroller.devicemanager.IDeviceService;
import net.floodlightcontroller.loadbalancer.LoadBalancer.SubActionStruct;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.restserver.IRestApiService;
import net.floodlightcontroller.routing.IRoutingService;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.staticflowentry.StaticFlowEntryPusher;
import net.floodlightcontroller.storage.AbstractStorageSource;
import net.floodlightcontroller.storage.IStorageSourceService;
import net.floodlightcontroller.storage.StorageSourceNotification;
import net.floodlightcontroller.topology.ITopologyService;


public class FlowAggr {

	private static class Node {
		Node leftChild;
		Node rightChild;
		int value;

		Node(int data) {
			leftChild = null;
			rightChild = null;
			value = data;
		}
	}
	private static class OpAction {
		String nw_dst;
		String tp_dst;
		String dl_dst;
		String output;
	}
	protected static Logger log = LoggerFactory.getLogger(LoadBalancer.class);
	private static ArrayList<String> routes=new ArrayList<String>();
	protected IFloodlightProviderService floodlightProvider;
	public OpAction getOpaction(String actionstr)
	{
		//[SET_NW_DST[10.1.2.4], SET_TP_DST[80], SET_DL_DST[00:00:00:00:00:02], OUTPUT[2]]
		OpAction op=new OpAction();
		String str=actionstr.substring(1, actionstr.length()-2);
		str=str.replace("]", "");
		str=str.replace("[", "=");
		str=str.replace(" ", "");
		for(String item:str.split(","))
		{
			item.split("=");
			if (item.split("=")[0].equals("SET_NW_DST")) 
			{
				op.nw_dst=item.split("=")[1];
				continue;
			}
			if (item.split("=")[0].equals("SET_TP_DST")) 
			{
				op.tp_dst=item.split("=")[1];
				continue;
			}
			if (item.split("=")[0].equals("SET_DL_DST")) 
			{
				op.dl_dst=item.split("=")[1];
				continue;
			}
			if (item.split("=")[0].equals("OUTPUT")) 
			{
				op.output=item.split("=")[1];
				continue;
			}
		}
		return op;
	}
	public String toActionstr(OpAction op)
	{
		//SET_NW_DST=10.1.2.4, SET_TP_DST=80, SET_DL_DST=00:00:00:00:00:02, OUTPUT=2
		return "";
		
	}
	public void aggr(String dpid,Map<String, OFFlowMod> mf,IStaticFlowEntryPusherService sfp,HashMap<String, LBPool> pools)
	{
		HashMap<String, OFFlowMod> hm=new HashMap<String, OFFlowMod>();
		HashMap<String, ArrayList<Integer>> ips=new HashMap<String, ArrayList<Integer>>();
		HashMap<String, ArrayList<String>> ops=new HashMap<String, ArrayList<String>>();
		ArrayList<String> tlist=new ArrayList<String>();
		Iterator iter = mf.entrySet().iterator();
    	while (iter.hasNext()) {
    		Map.Entry entry = (Map.Entry) iter.next();
    		Object key = entry.getKey();
    		OFFlowMod of = (OFFlowMod)(entry.getValue());
    		String ename=(String)key;
    		if(ename.indexOf("inbound")!=0)
    			continue;
    		ename=ename.split("-")[2]+":"+ename.split("-")[4];
    		if(!tlist.contains(ename))
    		{
    			tlist.add(ename);
    			
    		}
    		hm.put(ename, of);
    		ArrayList<Integer> tt=new ArrayList<Integer>();
    		ArrayList<String> ttop=new ArrayList<String>();
    		if(!ips.containsKey(ename))
    		{
    			tt.add(of.getMatch().getNetworkSource());
    			ips.put(ename, tt);
    			ttop.add(of.getActions().toString());
    			ops.put(ename,ttop);
    		}
    		else	
    		{
    			tt=ips.get(ename);
    			tt.add(of.getMatch().getNetworkSource());
    			ips.put(ename, tt);
    			
    			ttop=ops.get(ename);
    			if(!ttop.contains(of.getActions().toString()))
    			{
    				ttop.add(of.getActions().toString());
    				ops.put(ename, ttop);
    			}
    		}
    	}
    	for(String ipport:tlist)
    	{
    		if(!pools.containsKey(ipport))
    			continue;
    		ArrayList<String> afteraggr=createBinTree(ips.get(ipport));
    		int pos=0;
    		if(afteraggr.size()==ips.get(ipport).size())//means there is no aggregation
    		{
    			continue;
    		}
    		else
    		{
    			
    			for(String str:afteraggr)
    			{
    				if(pos>ops.get(ipport).size()-1)
    				{
    					pos=0;
    				}
    				OpAction op=getOpaction(ops.get(ipport).get(pos));
    				pos=pos+1;
    				
    				
    				OFFlowMod fm =new OFFlowMod();

    	               fm.setIdleTimeout((short) 360);   // same as ODL version
    	               fm.setHardTimeout((short) 0);   // 
    	               fm.setBufferId(OFPacketOut.BUFFER_ID_NONE);
    	               fm.setCommand((short) 0);
    	               fm.setFlags((short) 0);
    	               fm.setOutPort(OFPort.OFPP_NONE.getValue());
    	               fm.setCookie((long) 0);  
    	               fm.setPriority((short)9);
    	            String entryName;
    	            String matchString = null;
    	            String actionString = null;
    	            
    	            //in
    	            entryName="In-Aggr-"+str+""+"-"+ipport;
    	            matchString = "nw_src="+str+","
    	            				+"nw_dst="+IPv4.fromIPv4Address(Integer.parseInt(ipport.split(":")[0]))+","
    	            				+"tp_dst="+ipport.split(":")[1]+","
    	                           + "nw_proto=6,"
    	                           + "dl_type=2048,";
    	                            //    + "in_port=";
    	            actionString = "set-dst-ip="+op.nw_dst+"," 
    	                     		   + "set-dst-port="+op.tp_dst+","
    	                                 + "set-dst-mac="+op.dl_dst+","
    	                                 + "output="+op.output;
    	            
    	            net.floodlightcontroller.loadbalancer.LoadBalancer.parseActionString(fm, actionString, log);

    	               OFMatch ofMatch = new OFMatch();
    	              // ofMatch.setWildcards(Wildcards.FULL.matchOn(Wildcards.Flag.NW_SRC).withNwSrcMask(24));
    	               try {
    	                   ofMatch.fromString(matchString);
    	               } catch (IllegalArgumentException e) {
    	                   
    	               }
    	               //ofMatch.setWildcards(Wildcards.FULL.matchOn(Wildcards.Flag.NW_SRC).withNwSrcMask(24));
    	               
    	               fm.setMatch(ofMatch);
    	               
    	               sfp.addFlow(entryName, fm, dpid);
    	               
    	            //out 
    	               entryName="Out-Aggr-"+str+"-"+ipport;
       	            matchString = "nw_dst="+str+","
       	            			+ "nw_src="+op.nw_dst+","
       	            			+ "tp_src="+op.tp_dst+","
       	                           + "nw_proto=6,"
       	                           + "dl_type=2048,";
       	                            //    + "in_port=";
       	            actionString = "set-src-ip="+pools.get(ipport).getVip()+"," 
       	                     		   + "set-src-port="+pools.get(ipport).getPort()+","
       	                                 + "set-src-mac="+pools.get(ipport).getVipMac()+","
       	                                 + "output="+hm.get(ipport).getMatch().getInputPort();
       	            
       	            net.floodlightcontroller.loadbalancer.LoadBalancer.parseActionString(fm, actionString, log);
       	         ofMatch = new OFMatch();
       	               try {
       	                   ofMatch.fromString(matchString);
       	               } catch (IllegalArgumentException e) {
       	                   
       	               }
       	               fm.setMatch(ofMatch);
       	               sfp.addFlow(entryName, fm, dpid);
    	            
    				/*
    				//in
    				String entryName="In-Aggr-"+str+"";
    				OFFlowMod tof=hm.get(ipport);
    				tof.setPriority((short)5);
    				tof.setIdleTimeout((short)3600);
    				tof.setCookie(0);
    				OFMatch ofMatch=tof.getMatch();
    				String inp=ofMatch.getInputPort()+"";
    				String actionstr=tof.getActions().toString();
    				OpAction op=getOpaction(actionstr);
    				//actionstr="set-dst-ip="+op.nw_dst+",set-dst-port="+op.tp_dst+",set-dst-mac="+op.dl_dst+",output="+op.output;
    				actionstr=actionstr.substring(1, actionstr.length()-2);
    				actionstr=actionstr.replace("[", "=");
    				actionstr=actionstr.replace("]", "");
    				actionstr=actionstr.replace("SET_NW_DST", "set-dst-ip");
    				actionstr=actionstr.replace("SET_TP_DST", "set-dst-port");
    				actionstr=actionstr.replace("SET_DL_DST", "set-dst-mac");
    				actionstr=actionstr.replace("OUTPUT", "output");
    				actionstr=actionstr.replace(" ", "");
    				 
    				
    				
    				Map<String, Object> rowValues=new HashMap<String, Object>();
    				rowValues.put("name", entryName);
    				rowValues.put("nw_src", str);
    				rowValues.put("idleTimeout", "3600");
    				//rowValues.put("cookie", 0);
    				rowValues.put("active", "true");
    				rowValues.put("priority", "9");
    				rowValues.put("actions", actionstr);
    				rowValues.put("switch_id", dpid);
    				rowValues.put("dl_type", 2048);
    				rowValues.put("nw_proto", 6);
    				sss.insertRowAsync(StaticFlowEntryPusher.TABLE_NAME, rowValues);
    				
    				//out
    				/*entryName="Out-Aggr-"+str+"";
    				actionstr="set-src-ip="+ipport.split(":")[0]+",set-src-port="+ipport.split(":")[1]+",set-src-mac="+pools.get(ipport).getVipMac()+",output="+inp;
    				rowValues=new HashMap<String, Object>();
    				rowValues.put("name", entryName);
    				rowValues.put("nw_dst", str);
    				rowValues.put("nw_src", op.nw_dst);
    				//rowValues.put("tp_src", String.valueOf(op.tp_dst & 0xffff));
    				rowValues.put("idleTimeout", "3600");
    				//rowValues.put("cookie", 0);
    				rowValues.put("active", "true");
    				rowValues.put("priority", "9");
    				rowValues.put("actions", actionstr);
    				rowValues.put("switch_id", dpid);
    				rowValues.put("dl_type", 2048);
    				rowValues.put("nw_proto", 6);
    				sss.insertRowAsync(StaticFlowEntryPusher.TABLE_NAME, rowValues);*/
    				
    			}
    		}
    	}
	}
	public ArrayList<String> createBinTree(ArrayList<Integer> iplist) {
		routes=new ArrayList<String>();
		Node root=new Node(2);
		for(int ip:iplist)
		{
			String binstr=Integer.toBinaryString(ip);
			String zerostr="00000000000000000000000000000000";
			binstr=zerostr.substring(0, 32-binstr.length())+binstr;
			root=insert(root,binstr,0);
		}
		String[] path=new String[32];
		preOrderTraverse(root.leftChild,path,"0",0);
		preOrderTraverse(root.rightChild,path,"1",0);
		for(String str:routes)
		{
			System.out.print(str);
			System.out.println();
		}
		return routes;
	}
	private static String bintoip(String bin)
	{
		String t="";
		t=String.valueOf(Integer.valueOf(bin.substring(0, 8),2)&0xff)+".";
		t=t+String.valueOf(Integer.valueOf(bin.substring(8, 16),2)&0xff)+".";
		t=t+String.valueOf(Integer.valueOf(bin.substring(16, 24),2)&0xff)+".";
		t=t+String.valueOf(Integer.valueOf(bin.substring(24, 32),2)&0xff);
		return t;
	}
	private Node insert(Node node, String str,int level) {  
        if (node == null) {  
        	node = new Node(0);
        }
		if(level==32)
		{
			node.value=1;
			return node;
		}
        if (str.charAt(level)=='0') {  
                node.leftChild = insert(node.leftChild, str,level+1);  
            } else {  
                node.rightChild = insert(node.rightChild, str,level+1);   
        }  
        if(node.leftChild!=null && node.leftChild.value==1 && node.rightChild!=null && node.rightChild.value==1)
        {
        	node.value=1;
        }
        return node;  
    }  
	public static void preOrderTraverse(Node node,String[] path,String value,int level) {
		if (node == null || level==32)
			return;
		path[level]=value;
		if(node.value==1)
		{
			String t="";
			for(int i=0;i<=level;i++)
			{
				t=t+path[i];
			}
			String zerostr="00000000000000000000000000000000";
			t=bintoip(t+zerostr.substring(0, 31-level))+"/"+(level+1);
			routes.add(t);
			return;
		}
		preOrderTraverse(node.leftChild,path,"0",level+1);
		preOrderTraverse(node.rightChild,path,"1",level+1);
	}

}


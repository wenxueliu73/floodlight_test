package net.floodlightcontroller.loadbalancer;

import java.io.IOException;
import java.util.Collection;

import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceVlan extends ServerResource {

    protected static Logger log = LoggerFactory.getLogger(ResourceVlan.class);
    private ErrorCode ec=new ErrorCode();
    @Get("json")
    public Collection <LBVlan> retrieve() {
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());
        
        String vlanId = (String) getRequestAttributes().get("id");
        if (vlanId!=null && vlanId!="0")
            return lbs.getVlans(vlanId);
        else        
            return lbs.getVlans("0");               
    }
    
    @Post
    public Object createVlan(String postData) {        

        LBVlan vlan=new LBVlan();
        jsontoClass jtoclass=new jsontoClass();
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());
        int code=0;
        //create new pool
        try {
                vlan=jtoclass.jsonToVlan(postData);
                code=lbs.createVlan(vlan);
            } 
        catch (IOException e) {
                log.error("Could not parse JSON {}", e.getMessage());
                code=-99;
            }
        return "{\"result\" : \""+code+"\",\"msg\":\""+ec.getInfo(String.valueOf(code))+"\"}";
    }
    @Put
    public Object modifyVlan(String postData) {        

    	LBVlan vlan=new LBVlan();
        jsontoClass jtoclass=new jsontoClass();
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());
        
        String vlanId=(String) getRequestAttributes().get("id");
    	String bksId=(String) getRequestAttributes().get("bkid");
        int code=0;
        //update pool
        if(bksId==null||bksId=="")
        {
            try {
            	vlan=jtoclass.jsonToVlan(postData);
                vlan.setId(vlanId);
                code=lbs.updateVlan(vlan);
            } catch (IOException e) {
                log.error("Could not parse JSON {}", e.getMessage());
                code=-99;
            }

        }
        else
        //add bk server
        {
        	
        	code=lbs.addBkstovlan(vlanId, bksId);
        }
        return "{\"result\" : \""+code+"\",\"msg\":\""+ec.getInfo(String.valueOf(code))+"\"}";
    }
    
    @Delete
    public Object deleteVlan() {
        
        String vlanId = (String) getRequestAttributes().get("id");
               
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());
        String bksId=(String) getRequestAttributes().get("bkid");
        int code=0;
        if(bksId==null||bksId=="")
        {
        	code= lbs.deleteVlan(vlanId);
        }
        else
        {
        	code= lbs.delBksfromVlan(vlanId, bksId);
        }
        return "{\"result\" : \""+code+"\",\"msg\":\""+ec.getInfo(String.valueOf(code))+"\"}";
    }

   
    
}

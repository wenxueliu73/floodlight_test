package net.floodlightcontroller.loadbalancer;

public class PacketInfo {

    private int sip;
    private short sport;
    private int dip;
    private short dport;
    private byte protocol;

    public PacketInfo() {
    }

    public int getSip() {
        return sip;
    }

    public void setSip(int sip) {
        this.sip = sip;
    }

    public short getSport() {
        return sport;
    }

    public void setSport(short sport) {
        this.sport = sport;
    }

    public int getDip() {
        return dip;
    }

    public void setDip(int dip) {
        this.dip = dip;
    }

    public short getDport() {
        return dport;
    }

    public void setDport(short dport) {
        this.dport = dport;
    }

    public byte getProtocol() {
        return protocol;
    }

    public void setProtocol(byte protocol) {
        this.protocol = protocol;
    }

    @Override
    public String toString() {
        return "PacketInfo [sip=" + sip + ", sport=" + sport + ", dip=" + dip
                + ", dport=" + dport + ", protocol=" + protocol + "]";
    }

}

package net.floodlightcontroller.loadbalancer;

import java.util.ArrayList;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@JsonSerialize(using=LBVlanSerializer.class)
public class LBVlan {

	private String id;//vlan id
	private String name; //vlan name
    private int isactive; //0 disable 1 active
    private ArrayList<String> bksids = new ArrayList<String>(); // back servers id list
	

    public LBVlan(String name, int isactive) {
    	this.id=String.valueOf((int) (Math.random()*10000));
        this.name = name;
        this.isactive = isactive;
    }
    public String getId() {
		return id;
	}
	public ArrayList<String> getBksids() {
		return bksids;
	}
	public void setBksids(ArrayList<String> bksids) {
		this.bksids = bksids;
	}
	public int addBksids(String bksid)
	{
		if(this.bksids.contains(bksid))
			return -6;
		else
		{
			this.bksids.add(bksid);
			return 1;
		}
	}
	public int delBkids(String bksid)
	{
		if(!this.bksids.contains(bksid))
			return -1;
		else
		{
			this.bksids.remove(bksid);
			return 1;
		}
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	public LBVlan() {
    	this.id="";
        this.name = "";
        this.isactive = 1;
    }

}

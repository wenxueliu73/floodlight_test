package net.floodlightcontroller.loadbalancer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class LBVlanSerializer extends JsonSerializer<LBVlan>{

    @Override
    public void serialize(LBVlan vlan, JsonGenerator jGen,
                          SerializerProvider serializer) throws IOException,
                                                  JsonProcessingException {
        jGen.writeStartObject();
        
        jGen.writeStringField("id", vlan.getId());
        jGen.writeStringField("name", vlan.getName());
        jGen.writeStringField("isactive", String.valueOf(vlan.getIsactive()));

        StringBuilder bks = new StringBuilder();
        for (int i=0; i<vlan.getBksids().size(); i++) {
            bks.append(vlan.getBksids().get(i).toString());
            bks.append(",");
        }

        jGen.writeStringField("bkserver", bks.toString());
        jGen.writeEndObject();
    }

}

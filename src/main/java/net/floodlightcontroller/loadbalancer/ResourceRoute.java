package net.floodlightcontroller.loadbalancer;

import java.io.IOException;
import java.util.Collection;

import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceRoute extends ServerResource {

    protected static Logger log = LoggerFactory.getLogger(ResourceRoute.class);
    private ErrorCode ec=new ErrorCode();
    
    @Get("json")
    public Object retrieve() {
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());
        
        String poolId = (String) getRequestAttributes().get("id");
        int code=0;
        if(poolId!=null && poolId!="")
        {
        	lbs.routeAddr(poolId);
        	code=1;
        }
        else
        {
        	code=-99;
        }
        return "{\"result\" : \""+code+"\",\"msg\":\""+ec.getInfo(String.valueOf(code))+"\"}";
    }  
    
}

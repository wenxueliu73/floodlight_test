package net.floodlightcontroller.loadbalancer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class LBPoolSerializer extends JsonSerializer<LBPool>{

    @Override
    public void serialize(LBPool pool, JsonGenerator jGen,
                          SerializerProvider serializer) throws IOException,
                                                  JsonProcessingException {
        jGen.writeStartObject();
        
        jGen.writeStringField("id", pool.getId());
        jGen.writeStringField("name", pool.getName());
        jGen.writeStringField("vip", pool.getVip());
        jGen.writeStringField("vipMac", pool.getVipMac().toString());
        jGen.writeStringField("lbMethod", String.valueOf(pool.getLbMethod()));
        jGen.writeStringField("isactive", String.valueOf(pool.getIsactive()));
        jGen.writeStringField("protocol", String.valueOf(pool.getProtocol()));
        jGen.writeStringField("port", String.valueOf(pool.getPort()));

        StringBuilder bks = new StringBuilder();
        for (int i=0; i<pool.getServers().size(); i++) {
            bks.append(pool.getServers().get(i).getId());
            bks.append(",");
        }
        jGen.writeStringField("bkserver", bks.toString());
        jGen.writeEndObject();
    }

}

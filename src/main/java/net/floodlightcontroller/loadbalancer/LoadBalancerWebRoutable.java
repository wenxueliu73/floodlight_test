package net.floodlightcontroller.loadbalancer;

import org.restlet.Context;
import org.restlet.Restlet;
import org.restlet.routing.Router;

import net.floodlightcontroller.restserver.RestletRoutable;
import net.floodlightcontroller.virtualnetwork.NoOp;

public class LoadBalancerWebRoutable implements RestletRoutable {

    @Override
    public Restlet getRestlet(Context context) {
        Router router = new Router(context);
        router.attach("/pools", ResourcePool.class); // GET //POST
        router.attach("/pools/{id}", ResourcePool.class); // GET  PUT DELETE
       // router.attach("/pools/{type}", ResourcePool.class); // PUT,POST, DELETE
        //router.attach("/pools/{type}/{id}", ResourcePool.class); // PUT DELETE
        router.attach("/pools/{id}/{bkid}", ResourcePool.class); // PUT DELETE
       
        router.attach("/bkservers", ResourceBkServer.class); // GET //POST
        router.attach("/bkservers/{id}", ResourceBkServer.class); // GET //PUT //DELETE
        //router.attach("/bkservers/{type}/{id}", ResourceBkServer.class); // GET, PUT,POST, DELETE
        //router.attach("/bkservers/{type}/{id}/{ip}", ResourceBkServer.class); // PUT,POST
        //router.attach("/bkservers/{type}/{id}/{bkid}", ResourceBkServer.class); // DELETE
        
        router.attach("/vlans", ResourceVlan.class); // GET, POST
        router.attach("/vlans/{id}", ResourceVlan.class); // GET  PUT DELETE
        //router.attach("/vlans/{type}/{id}", ResourceVlan.class); // PUT,DELETE
        router.attach("/vlans/{id}/{bkid}", ResourceVlan.class); // PUT DELETE
        
        router.attach("/routes/{id}", ResourceRoute.class); // GET  PUT DELETE
        router.attachDefault(NoOp.class);
        return router;
     }

    @Override
    public String basePath() {
        return "/vm/lb";
    }

}

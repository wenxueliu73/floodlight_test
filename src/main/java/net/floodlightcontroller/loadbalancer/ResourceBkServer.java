package net.floodlightcontroller.loadbalancer;

import java.io.IOException;
import java.util.Collection;






import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceBkServer extends ServerResource {

    protected static Logger log = LoggerFactory.getLogger(ResourceBkServer.class);
    private ErrorCode ec=new ErrorCode();
    @Get("json")
    public Collection <LBBkServers> retrieve() {
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());
        
        String bksId = (String) getRequestAttributes().get("id");
        if (bksId!=null && bksId!="0")
            return lbs.getBkServers(bksId);
        else        
            return lbs.getBkServers("0");               
    }
    
    @Post
    public Object createBkServer(String postData) {        

        LBBkServers bks=new LBBkServers();
        jsontoClass jtoclass=new jsontoClass();
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());
        
        int code=0;
        //create new back server
        try {
                bks=jtoclass.jsonToBks(postData);
                code= lbs.createBkServer(bks);
        } catch (IOException e) {
                log.error("Could not parse JSON {}", e.getMessage());
                code=-99;
        }
            
        return "{\"result\" : \""+code+"\",\"msg\":\""+ec.getInfo(String.valueOf(code))+"\"}";
    }
    @Put
    public Object modifyBkServer(String postData) {        

        LBBkServers bks=new LBBkServers();
        jsontoClass jtoclass=new jsontoClass();
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());
        
        int code=0;
        //update back server
        String bksId=(String) getRequestAttributes().get("id");
        try {
            	bks=jtoclass.jsonToBks(postData);
            	bks.setId(bksId);
            	code= lbs.updateBkServer(bks);
            } catch (IOException e) {
                log.error("Could not parse JSON {}", e.getMessage());
                code=-99;
        }
        return "{\"result\" : \""+code+"\",\"msg\":\""+ec.getInfo(String.valueOf(code))+"\"}";
    }
    
    @Delete
    public Object deleteBkServer() {
        
        String bksId = (String) getRequestAttributes().get("id");
               
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());

        int code = lbs.deleteBkServer(bksId);
        return "{\"result\" : \""+code+"\",\"msg\":\""+ec.getInfo(String.valueOf(code))+"\"}";

    }

   
    
}

package net.floodlightcontroller.loadbalancer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class LBBkServersSerializer extends JsonSerializer<LBBkServers>{

    @Override
    public void serialize(LBBkServers server, JsonGenerator jGen,
                          SerializerProvider serializer) throws IOException,
                                                  JsonProcessingException {
        jGen.writeStartObject();
        
        jGen.writeStringField("id", server.getId());
        jGen.writeStringField("name", server.getName());
        jGen.writeStringField("ip", server.getIp());
        jGen.writeStringField("port", String.valueOf(server.getPort()));
        jGen.writeStringField("priority",String.valueOf(server.getPriority()));
        jGen.writeStringField("isactive", String.valueOf(server.getIsactive()));
        jGen.writeEndObject();
    }

}

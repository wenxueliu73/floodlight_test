package net.floodlightcontroller.loadbalancer;

import java.io.IOException;
import java.util.Collection;

import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourcePool extends ServerResource {

    protected static Logger log = LoggerFactory.getLogger(ResourcePool.class);
    private ErrorCode ec=new ErrorCode();
    
    @Get("json")
    public Collection <LBPool> retrieve() {
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());
        
        String poolId = (String) getRequestAttributes().get("id");
        //lbs.routeAddr(poolId);
        if (poolId!=null && poolId!="0")
            return lbs.getPools(poolId);
        else        
            return lbs.getPools("0");               
    }
    
    @Post
    public Object createPool(String postData) {        

        LBPool pool=new LBPool();
        jsontoClass jtoclass=new jsontoClass();
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());
        int code=0;
        //create new pool
        try {
                pool=jtoclass.jsonToPool(postData);
                code=lbs.createPool(pool);
            } 
        catch (IOException e) {
                log.error("Could not parse JSON {}", e.getMessage());
                code=-99;
            }
        return "{\"result\" : \""+code+"\",\"msg\":\""+ec.getInfo(String.valueOf(code))+"\"}";
    }
    @Put
    public Object modifyPool(String postData) {        

        LBPool pool=new LBPool();
        jsontoClass jtoclass=new jsontoClass();
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());
        
        //String type=(String) getRequestAttributes().get("type");
        int code=0;
        String poolId=(String) getRequestAttributes().get("id");
    	String bksId=(String) getRequestAttributes().get("bkid");
        //update pool
        if(bksId==null||bksId=="")
        {
            try {
                pool=jtoclass.jsonToPool(postData);
                pool.setId(poolId);
                code=lbs.updatePool(pool);
            } catch (IOException e) {
                log.error("Could not parse JSON {}", e.getMessage());
                code=-99;
            }

        }
        else
        //add bk server
        {
        	
        	code=lbs.addBkstoPool(poolId,bksId);
        }
        return "{\"result\" : \""+code+"\",\"msg\":\""+ec.getInfo(String.valueOf(code))+"\"}";
    }
    
    @Delete
    public Object deletePool() {
        
        String poolId = (String) getRequestAttributes().get("id");
               
        ILoadBalancerService lbs =
                (ILoadBalancerService)getContext().getAttributes().
                    get(ILoadBalancerService.class.getCanonicalName());
        String bksId=(String) getRequestAttributes().get("bkid");
        int code=0;
        if(bksId==null||bksId=="")
        {
        	code= lbs.deletePool(poolId);
        }
        else
        {
        	code= lbs.delBksfromPool(poolId,bksId);
        }
        return "{\"result\" : \""+code+"\",\"msg\":\""+ec.getInfo(String.valueOf(code))+"\"}";
    }

   
    
}

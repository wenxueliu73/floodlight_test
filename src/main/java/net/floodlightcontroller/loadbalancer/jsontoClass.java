package net.floodlightcontroller.loadbalancer;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.MappingJsonFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class jsontoClass
{
	protected static Logger log = LoggerFactory.getLogger(jsontoClass.class);
	
	public LBPool jsonToPool(String json) throws IOException {
	        if (json==null) return null;
	
	        MappingJsonFactory f = new MappingJsonFactory();
	        JsonParser jp;
	        LBPool item = new LBPool();
	        item.setId("");
	        item.setVip("-999");
	        item.setIsactive(-999);
	        item.setLbMethod(-999);
	        item.setPort((short)-999);
	        item.setProtocol(-999);
	        
	        
	        
	        try {
	            jp = f.createJsonParser(json);
	        } catch (JsonParseException e) {
	            throw new IOException(e);
	        }
	        
	        jp.nextToken();
	        if (jp.getCurrentToken() != JsonToken.START_OBJECT) {
	            throw new IOException("Expected START_OBJECT");
	        }
	        
	        while (jp.nextToken() != JsonToken.END_OBJECT) {
	            if (jp.getCurrentToken() != JsonToken.FIELD_NAME) {
	                throw new IOException("Expected FIELD_NAME");
	            }
	            
	            String n = jp.getCurrentName();
	            jp.nextToken();
	            if (jp.getText().equals("")) 
	                continue;
	            if (n.equals("id")) {
	            	item.setId(jp.getText());
	                continue;
	            } 
	            if (n.equals("name")) {
	            	item.setName(jp.getText());
	                continue;
	            }
	            if (n.equals("vip")) {
	            	if(checkIP(jp.getText()))
	            		item.setVip(jp.getText());
	                continue;
	            }
	            if (n.equals("lbMethod")) {
	            	log.info(jp.getText());
	               if(jp.getText().equals("0") || jp.getText().equals("1")|| jp.getText().equals("2"))
	               {
	            		item.setLbMethod(Integer.parseInt(jp.getText()));
	               }
	               continue;
	           }
	            if (n.equals("isactive")) {
	            	if(jp.getText().equals("0") || jp.getText().equals("1"))
	            		item.setIsactive(Integer.parseInt(jp.getText()));
	               continue;
	           }
	            if (n.equals("protocol")) {
	            	if(jp.getText().equals("0") || jp.getText().equals("1") || jp.getText()=="2")
	            	item.setProtocol(Integer.parseInt(jp.getText()));
	               continue;
	           }
	            if (n.equals("port")) {
	            	if(checkPort(jp.getText()))
	            		item.setPort(Short.parseShort(jp.getText()));
	               continue;
	           }
	            
	            
	            log.warn("Unrecognized field {} in " +
	                    "parsing Pools", 
	                    jp.getText());
	        }
	        jp.close();
	
	        return item;
	  }
	
	public LBBkServers jsonToBks(String json) throws IOException {
        if (json==null) return null;

        MappingJsonFactory f = new MappingJsonFactory();
        JsonParser jp;
        LBBkServers item = new LBBkServers();
        item.setIp("-999");
        item.setIsactive(-999);
        item.setPort((short)-999);
        item.setPriority(-999);
        try {
            jp = f.createJsonParser(json);
        } catch (JsonParseException e) {
            throw new IOException(e);
        }
        
        jp.nextToken();
        if (jp.getCurrentToken() != JsonToken.START_OBJECT) {
            throw new IOException("Expected START_OBJECT");
        }
        
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            if (jp.getCurrentToken() != JsonToken.FIELD_NAME) {
                throw new IOException("Expected FIELD_NAME");
            }
            
            String n = jp.getCurrentName();
            jp.nextToken();
            if (jp.getText().equals("")) 
                continue;
            if (n.equals("id")) {
            	item.setId(jp.getText());
                continue;
            } 
            if (n.equals("name")) {
            	item.setName(jp.getText());
                continue;
            }
            if (n.equals("ip")) {
            	if(checkIP(jp.getText()))
            		item.setIp(jp.getText());
                continue;
            }
            if (n.equals("port")) {
            	if(checkPort(jp.getText()))
            		item.setPort(Short.parseShort(jp.getText()));
               continue;
           }
            if (n.equals("isactive")) {
            	if(jp.getText().equals("0") || jp.getText().equals("1"))
            		item.setIsactive(Integer.parseInt(jp.getText()));
               continue;
           }
            if (n.equals("priority")) {
            	if(checkNum(jp.getText()))
            		item.setPriority(Integer.parseInt(jp.getText()));
               continue;
           }
           /* if (n.equals("vlanid")) {
           	    item.setVlanid(jp.getText());
               continue;
           }*/
            
            
            log.warn("Unrecognized field {} in " +
                    "parsing Bks", 
                    jp.getText());
        }
        jp.close();

        return item;
    }
	
	public LBVlan jsonToVlan(String json) throws IOException {
        if (json==null) return null;

        MappingJsonFactory f = new MappingJsonFactory();
        JsonParser jp;
        LBVlan item = new LBVlan();
        item.setIsactive(-999);
        try {
            jp = f.createJsonParser(json);
        } catch (JsonParseException e) {
            throw new IOException(e);
        }
        
        jp.nextToken();
        if (jp.getCurrentToken() != JsonToken.START_OBJECT) {
            throw new IOException("Expected START_OBJECT");
        }
        
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            if (jp.getCurrentToken() != JsonToken.FIELD_NAME) {
                throw new IOException("Expected FIELD_NAME");
            }
            
            String n = jp.getCurrentName();
            jp.nextToken();
            if (jp.getText().equals("")) 
                continue;
            if (n.equals("id")) {
            	item.setId(jp.getText());
                continue;
            } 
            if (n.equals("name")) {
            	item.setName(jp.getText());
                continue;
            }
            if (n.equals("isactive")) {
            	if(jp.getText().equals("0") || jp.getText().equals("1"))
            		item.setIsactive(Integer.parseInt(jp.getText()));
               continue;
           }
            
            log.warn("Unrecognized field {} in " +
                    "parsing Bks", 
                    jp.getText());
        }
        jp.close();

        return item;
    }
	public boolean checkPort(String str)
	{
		try
		{
			int t=Integer.valueOf(str) & 0xffff;
			if(t>0 && t<=65535)
				return true;
			else
				return false;
		}catch(Exception e){
			return false;
		}
	}
	public boolean checkNum(String str)
	{
		try
		{
			int t=Integer.valueOf(str);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	public boolean checkIP(String str)
	{
		String regex = "([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(str);
		return m.matches();
	}
}
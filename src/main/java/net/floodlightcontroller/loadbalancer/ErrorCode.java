package net.floodlightcontroller.loadbalancer;
import java.util.HashMap;
public class ErrorCode
{
	private HashMap<String,String> code=new HashMap<String,String>();
	public ErrorCode()
	{
		code.put("1", "successfully completed");
		code.put("-1", "VIp:VPort is exist");
		code.put("-2", "BksIP:BksPort is exist");
		code.put("-3", "Pool is not exist");
		code.put("-4", "Back server is not exist");
		code.put("-5", "Vlan is not exist");
		code.put("-6", "Back server has been added to this Vlan");
		code.put("-7", "Back server has been added to this Pool");
		code.put("-8", "Back server has been added to one Pool");
		code.put("-9", "Vlanid does not match BKid");
		code.put("-10", "Poolid does not match BKid");
		code.put("-99", "Could not parse JSON");
		code.put("-100", "IP Address is invalid");
		code.put("-101", "Port is invalid");
		code.put("-102", "Priority is invalid");
		code.put("-103", "IsActive is invalid");
		code.put("-104", "LbMethod is invalid");
		code.put("-105", "Protocol is invalid");
		
	}
	public HashMap<String, String> getCode() {
		return code;
	}
	public void setCode(HashMap<String, String> code) {
		this.code = code;
	}
	public String getInfo(String str)
	{
		return this.code.get(str);
	}
}

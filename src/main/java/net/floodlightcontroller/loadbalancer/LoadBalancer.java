package net.floodlightcontroller.loadbalancer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Random;

import org.openflow.protocol.OFFlowMod;
import org.openflow.protocol.OFMatch;
import org.openflow.protocol.OFMessage;
import org.openflow.protocol.OFPacketIn;
import org.openflow.protocol.OFPacketOut;
import org.openflow.protocol.OFPort;
import org.openflow.protocol.OFType;
import org.openflow.protocol.Wildcards;
import org.openflow.protocol.action.OFAction;
import org.openflow.protocol.action.OFActionDataLayerDestination;
import org.openflow.protocol.action.OFActionDataLayerSource;
import org.openflow.protocol.action.OFActionEnqueue;
import org.openflow.protocol.action.OFActionNetworkLayerDestination;
import org.openflow.protocol.action.OFActionNetworkLayerSource;
import org.openflow.protocol.action.OFActionNetworkTypeOfService;
import org.openflow.protocol.action.OFActionOutput;
import org.openflow.protocol.action.OFActionStripVirtualLan;
import org.openflow.protocol.action.OFActionTransportLayerDestination;
import org.openflow.protocol.action.OFActionTransportLayerSource;
import org.openflow.protocol.action.OFActionVirtualLanIdentifier;
import org.openflow.protocol.action.OFActionVirtualLanPriorityCodePoint;
import org.openflow.util.HexString;
import org.openflow.util.U16;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IOFMessageListener;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.counter.ICounterStoreService;
import net.floodlightcontroller.devicemanager.IDevice;
import net.floodlightcontroller.devicemanager.IDeviceService;
import net.floodlightcontroller.devicemanager.SwitchPort;
import net.floodlightcontroller.packet.ARP;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.ICMP;
import net.floodlightcontroller.packet.IPacket;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.packet.TCP;
import net.floodlightcontroller.packet.UDP;
import net.floodlightcontroller.restserver.IRestApiService;
import net.floodlightcontroller.routing.IRoutingService;
import net.floodlightcontroller.routing.Route;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.storage.IStorageSourceService;
import net.floodlightcontroller.topology.ITopologyService;
import net.floodlightcontroller.topology.NodePortTuple;
import net.floodlightcontroller.util.MACAddress;
import net.floodlightcontroller.util.OFMessageDamper;

public class LoadBalancer implements IFloodlightModule,
    ILoadBalancerService, IOFMessageListener {

    protected static Logger log = LoggerFactory.getLogger(LoadBalancer.class);

    //Copied from default loadbalancer module. 
    //Our dependencies
    protected IFloodlightProviderService floodlightProvider;
    protected IRestApiService restApi;
    
    protected ICounterStoreService counterStore;
    protected OFMessageDamper messageDamper;
    protected IDeviceService deviceManager;
    protected IRoutingService routingEngine;
    protected ITopologyService topology;
    protected IStaticFlowEntryPusherService sfp;
    protected IStorageSourceService storage;
    
    
    private HashMap<String, String> clientServerMap=new HashMap<String,String>();
   // private HashMap<Integer, Integer> nextBkServer=new HashMap<Integer,Integer>();
    protected HashMap<String, LBPool> pools = new HashMap<String, LBPool>();
    protected HashMap<Integer, MACAddress> vipArp = new HashMap<Integer, MACAddress>();
    private HashMap<String,LBBkServers> bkservers = new HashMap<String,LBBkServers>();
    private HashMap<String,LBVlan> vlans=new HashMap<String,LBVlan>();
   // private HashMap<Integer, String> vips = new HashMap<Integer, String>();
    private Random randomGenerator = new Random();
    
    

    
    //Copied from Forwarding with message damper routine for pushing proxy Arp 
    protected static int OFMESSAGE_DAMPER_CAPACITY = 10000; // ms. 
    protected static int OFMESSAGE_DAMPER_TIMEOUT = 250; // ms 
    protected static String LB_ETHER_TYPE = "0x800";
    private static short LB_IPSWITCH_PRIORITY = 10;
    
    // Comparator for sorting by SwitchCluster
    public Comparator<SwitchPort> clusterIdComparator =
            new Comparator<SwitchPort>() {
                @Override
                public int compare(SwitchPort d1, SwitchPort d2) {
                    Long d1ClusterId = 
                            topology.getL2DomainId(d1.getSwitchDPID());
                    Long d2ClusterId = 
                            topology.getL2DomainId(d2.getSwitchDPID());
                    return d1ClusterId.compareTo(d2ClusterId);
                }
            };
    
    @Override
    public String getName() {
        return "loadbalancer_v1";
    }

    @Override
    public boolean isCallbackOrderingPrereq(OFType type, String name) {
        return (type.equals(OFType.PACKET_IN) && 
                (name.equals("topology") || 
                 name.equals("devicemanager") ||
                 name.equals("virtualizer")));
    }

    @Override
    public boolean isCallbackOrderingPostreq(OFType type, String name) {
        return (type.equals(OFType.PACKET_IN) && name.equals("forwarding"));
   }

    @Override
    public net.floodlightcontroller.core.IListener.Command
            receive(IOFSwitch sw, OFMessage msg, FloodlightContext cntx) {
        switch (msg.getType()) {
            case PACKET_IN:
                return processPacketIn(sw, (OFPacketIn)msg, cntx);
            default:
                break;
        }
        log.warn("Received unexpected message {}", msg);
        return Command.CONTINUE;
    }

    private net.floodlightcontroller.core.IListener.Command
            processPacketIn(IOFSwitch sw, OFPacketIn pi,
                            FloodlightContext cntx) {
        
        Ethernet eth = IFloodlightProviderService.bcStore.get(cntx,
                                                              IFloodlightProviderService.CONTEXT_PI_PAYLOAD);
        IPacket pkt = eth.getPayload();
        if (eth.isBroadcast() || eth.isMulticast()) {
            // handle ARP for VIP
            if (pkt instanceof ARP) {
                // retrieve arp to determine target IP address                                                       
                ARP arpRequest = (ARP) eth.getPayload();

                int desIp =IPv4.toIPv4Address(arpRequest.getTargetProtocolAddress());

                if (vipArp.containsKey(desIp)) {  //named the vip list as ODL
                    vipProxyArpReply(sw, pi, cntx, vipArp.get(desIp));
                    return Command.STOP;
                }
            }
        } else {
           
            if (pkt instanceof IPv4) {
                IPv4 ip_pkt = (IPv4) pkt;
                int desIp = ip_pkt.getDestinationAddress();
                int nextstep=0;
                PacketInfo pinfo= new PacketInfo();
                if (ip_pkt.getPayload() instanceof TCP) {
                    TCP tcp_pkt = (TCP) ip_pkt.getPayload();
                	if(!pools.containsKey(desIp+":"+tcp_pkt.getDestinationPort()))
                	{
                		return Command.CONTINUE;
                	}
                	nextstep=1;
                    pinfo.setSport(tcp_pkt.getSourcePort());
                    pinfo.setDport(tcp_pkt.getDestinationPort());
                    
                }
                if (ip_pkt.getPayload() instanceof UDP) {
                    UDP udp_pkt = (UDP) ip_pkt.getPayload();
                    if(!pools.containsKey(desIp+":"+udp_pkt.getDestinationPort()))
                	{
                    	return Command.CONTINUE;
                	}
                    nextstep=1;
                    pinfo.setSport(udp_pkt.getSourcePort());
                    pinfo.setDport(udp_pkt.getDestinationPort());
                }
                
                //It's not available due to the key is ip:port
                //only for testing
                
                if (ip_pkt.getPayload() instanceof ICMP) {
                	if(!pools.containsKey(desIp+":80"))
                	{
                		return Command.CONTINUE;
                	}
                	pinfo.setSport((short)8);
                    pinfo.setDport((short)0);
                    nextstep=1;
                }
                
                if(nextstep==0)
                	return Command.CONTINUE;


                pinfo.setDip(desIp);
                pinfo.setSip(ip_pkt.getSourceAddress());
                pinfo.setProtocol(ip_pkt.getProtocol());
                
             // for chosen member, check device manager and find and push routes, in both directions
                // same function from ODL version
                String poolkey=pinfo.getDip()+":"+pinfo.getDport();
                
                //only for icmp teting
                if(pinfo.getDport()==0)
                	poolkey=pinfo.getDip()+":80";
                
                String bkserver=getBkServerForClient(pinfo,pools.get(poolkey));
                pushBidirectionalVipRoutes(sw, pi, cntx, pinfo,bkserver,pools.get(pinfo.getDip()+":"+pinfo.getDport()).getVipMac().toString());
               
                // packet out based on table rule. 
                // Function addLBFlow in ODL version 
                pushPacket(pkt, sw, pi.getBufferId(), pi.getInPort(), OFPort.OFPP_TABLE.getValue(),
                            cntx, true);

                return Command.STOP;
                
            }
        }
        // bypass non-load-balanced traffic for normal processing (forwarding)
        return Command.CONTINUE;
    }

    //Choice next back server.
    public String getBkServerForClient(PacketInfo pi, LBPool pool) {
        syncWithLoadBalancerData(pool);
        String bs="";
        String client = String.valueOf(pi.getSip())+String.valueOf(pi.getSport())+String.valueOf(pi.getDip())+String.valueOf(pi.getDport());
        if (this.clientServerMap!=null && this.clientServerMap.containsKey(client)) {
            bs = this.clientServerMap.get(client);
            //log.info("LB Send to the lastest back server"+ bs);
        } else {
            if (pool.getLbMethod() == 0)// RoundRobin
            {
            	int pos;
            	//log.info("RoundRobin");
                pos=pool.getPos();
                if(pos>pool.getServermapkey().size()-1)
                {
                	pos=0;
                }
                bs=pool.getServermapkey().get(pos);
                pos=pos+1;
                pool.setPos(pos);
                this.pools.put(pi.getDip()+":"+pi.getDport(), pool);
                /*if (this.nextBkServer!=null && this.nextBkServer.containsKey(pi.getDip())) {
                    int pos = this.nextBkServer.get(pi.getDip());
                    bs = pool.getServers().get(pos).getIp();
                    pos = pos + 1;
                    if (pos > pool.getServers().size() - 1)
                        pos = 0;
                    this.nextBkServer.put(pi.getDip(), pos);
                } else {
                    bs = pool.getServers().get(0).getIp();
                    if (1 > pool.getServers().size() - 1)
                        this.nextBkServer.put(pi.getDip(), 0);
                    else
                        this.nextBkServer.put(pi.getDip(), 1);

                }*/
            } else// Random
            {
            	
            	int pos=0;
            	if(pool.getLbMethod() == 1)
            	{
            		pos = this.randomGenerator.nextInt(pool.getServermapkey().size());
            		//log.info("Random "+pos);
                    bs=pool.getServermapkey().get(pos);
            	}
            	if(pool.getLbMethod() == 2)//with priority
            	{
            		pos = this.randomGenerator.nextInt(pool.getServermapkeyplus().size());
            		//log.info("Random with priority "+pos);
                    bs=pool.getServermapkeyplus().get(pos);
            	}
               
            }

            this.clientServerMap.put(client, bs);
            //log.info("Directed to back server: "+ bs);
        }
        return bs;
    }
    private void syncWithLoadBalancerData(LBPool pool) {
        ArrayList<String> removeMap = new ArrayList<String>();
        if (this.clientServerMap!=null && this.clientServerMap.size() != 0) {
            for (String client : this.clientServerMap.keySet()) {

                if (!pool.getServersmap().containsKey(
                        this.clientServerMap.get(client))
                        || pool.getServersmap()
                                .get(this.clientServerMap.get(client))
                                .getIsactive() == 0) {// the back server has
                                                        // been deleted or
                                                        // disabled
                    removeMap.add(client);
                }
            }
        }

        for (String client : removeMap) {
            this.clientServerMap.remove(client);
        }
    }
    /**
     * used to send proxy Arp for load balanced service requests
     * @param IOFSwitch sw
     * @param OFPacketIn pi
     * @param FloodlightContext cntx
     * @param String vipId
     */
    
    protected void vipProxyArpReply(IOFSwitch sw, OFPacketIn pi, FloodlightContext cntx, MACAddress vipmac) {
        //log.info("vipProxyArpReply");
            
        Ethernet eth = IFloodlightProviderService.bcStore.get(cntx,
                                                              IFloodlightProviderService.CONTEXT_PI_PAYLOAD);

        // retrieve original arp to determine host configured gw IP address                                          
        if (! (eth.getPayload() instanceof ARP))
            return;
        ARP arpRequest = (ARP) eth.getPayload();
        
        // have to do proxy arp reply since at this point we cannot determine the requesting application type
        byte[] vipProxyMacBytes =vipmac.toBytes();
        
        // generate proxy ARP reply
        IPacket arpReply = new Ethernet()
            .setSourceMACAddress(vipProxyMacBytes)
            .setDestinationMACAddress(eth.getSourceMACAddress())
            .setEtherType(Ethernet.TYPE_ARP)
            .setVlanID(eth.getVlanID())
            .setPriorityCode(eth.getPriorityCode())
            .setPayload(
                new ARP()
                .setHardwareType(ARP.HW_TYPE_ETHERNET)
                .setProtocolType(ARP.PROTO_TYPE_IP)
                .setHardwareAddressLength((byte) 6)
                .setProtocolAddressLength((byte) 4)
                .setOpCode(ARP.OP_REPLY)
                .setSenderHardwareAddress(vipProxyMacBytes)
                .setSenderProtocolAddress(
                        arpRequest.getTargetProtocolAddress())
                .setTargetHardwareAddress(
                        eth.getSourceMACAddress())
                .setTargetProtocolAddress(
                        arpRequest.getSenderProtocolAddress()));
                
        // push ARP reply out
        pushPacket(arpReply, sw, OFPacketOut.BUFFER_ID_NONE, OFPort.OFPP_NONE.getValue(),
                   pi.getInPort(), cntx, true);
        
        return;
    }

    /**
     * used to push any packet - borrowed routine from Forwarding
     * 
     * @param OFPacketIn pi
     * @param IOFSwitch sw
     * @param int bufferId
     * @param short inPort
     * @param short outPort
     * @param FloodlightContext cntx
     * @param boolean flush
     */    
    public void pushPacket(IPacket packet, 
                           IOFSwitch sw,
                           int bufferId,
                           short inPort,
                           short outPort, 
                           FloodlightContext cntx,
                           boolean flush) {
        if (log.isTraceEnabled()) {
            log.trace("PacketOut srcSwitch={} inPort={} outPort={}", 
                      new Object[] {sw, inPort, outPort});
        }
        //log.info("PacketOut srcSwitch={} inPort={} outPort={}", 
        //           new Object[] {sw, inPort, outPort});

        OFPacketOut po =
                (OFPacketOut) floodlightProvider.getOFMessageFactory()
                                                .getMessage(OFType.PACKET_OUT);

        // set actions
        List<OFAction> actions = new ArrayList<OFAction>();
        actions.add(new OFActionOutput(outPort, (short) 0xffff));

        po.setActions(actions)
          .setActionsLength((short) OFActionOutput.MINIMUM_LENGTH);
        short poLength =
                (short) (po.getActionsLength() + OFPacketOut.MINIMUM_LENGTH);

        // set buffer_id, in_port
        po.setBufferId(bufferId);
        po.setInPort(inPort);

        // set data - only if buffer_id == -1
        if (po.getBufferId() == OFPacketOut.BUFFER_ID_NONE) {
            if (packet == null) {
                log.error("BufferId is not set and packet data is null. " +
                          "Cannot send packetOut. " +
                        "srcSwitch={} inPort={} outPort={}",
                        new Object[] {sw, inPort, outPort});
                return;
            }
            byte[] packetData = packet.serialize();
            poLength += packetData.length;
            po.setPacketData(packetData);
        }

        po.setLength(poLength);

        try {
            counterStore.updatePktOutFMCounterStoreLocal(sw, po);
            messageDamper.write(sw, po, cntx, flush);
        } catch (IOException e) {
            log.error("Failure writing packet out", e);
        }
    }

    /**
     * used to find and push in-bound and out-bound routes using StaticFlowEntryPusher
     * @param IOFSwitch sw
     * @param OFPacketIn pi
     * @param FloodlightContext cntx
     * @param IPClient client
     * @param LBMember member
     */
    protected void pushBidirectionalVipRoutes(IOFSwitch sw, OFPacketIn pi, FloodlightContext cntx, PacketInfo pinfo, String bkserver,String vipMac) {
        
        // borrowed code from Forwarding to retrieve src and dst device entities
        // Check if we have the location of the destination
        IDevice srcDevice = null;
        IDevice dstDevice = null;
        String bksinfo[]=bkserver.split(":");
        // retrieve all known devices
        Collection<? extends IDevice> allDevices = deviceManager
                .getAllDevices();
        
        for (IDevice d : allDevices) {
            for (int j = 0; j < d.getIPv4Addresses().length; j++) {
                    if (srcDevice == null && pinfo.getSip() == d.getIPv4Addresses()[j])
                        srcDevice = d;
                    if (dstDevice == null && bksinfo[0].equals(IPv4.fromIPv4Address(d.getIPv4Addresses()[j]))) {
                        dstDevice = d;
                       // member.macString = dstDevice.getMACAddressString();
                    }
                    if (srcDevice != null && dstDevice != null)
                        break;
            }
        }  
        
        // srcDevice and/or dstDevice is null, no route can be pushed
        if (srcDevice == null || dstDevice == null) {
            log.info("srcDeive or dstDevice null");
            return;
        }
        
        Long srcIsland = topology.getL2DomainId(sw.getId());

        if (srcIsland == null) {
            log.info("No openflow island found for source {}/{}", 
                      sw.getStringId(), pi.getInPort());
            return;
        }
        
        // Validate that we have a destination known on the same island
        // Validate that the source and destination are not on the same switchport
        boolean on_same_island = false;
        boolean on_same_if = false;
        for (SwitchPort dstDap : dstDevice.getAttachmentPoints()) {
            long dstSwDpid = dstDap.getSwitchDPID();
            Long dstIsland = topology.getL2DomainId(dstSwDpid);
            if ((dstIsland != null) && dstIsland.equals(srcIsland)) {
                on_same_island = true;
                if ((sw.getId() == dstSwDpid) &&
                        (pi.getInPort() == dstDap.getPort())) {
                    on_same_if = true;
                }
                break;
            }
        }
        
        if (!on_same_island) {
            // Flood since we don't know the dst device
            if (log.isTraceEnabled()) {
                log.trace("No first hop island found for destination " + 
                        "device {}, Action = flooding", dstDevice);
            }
            log.info("No first hop island found for destination " + 
                    "device {}, Action = flooding", dstDevice);
            return;
        }            
        
        if (on_same_if) {
            if (log.isTraceEnabled()) {
                log.trace("Both source and destination are on the same " + 
                        "switch/port {}/{}, Action = NOP", 
                        sw.toString(), pi.getInPort());
            }
            log.info("Both source and destination are on the same " + 
                    "switch/port {}/{}, Action = NOP", 
                    sw.toString(), pi.getInPort());
            return;
        }
        
        // Install all the routes where both src and dst have attachment
        // points.  Since the lists are stored in sorted order we can 
        // traverse the attachment points in O(m+n) time
        SwitchPort[] srcDaps = srcDevice.getAttachmentPoints();
        Arrays.sort(srcDaps, clusterIdComparator);
        SwitchPort[] dstDaps = dstDevice.getAttachmentPoints();
        Arrays.sort(dstDaps, clusterIdComparator);
        
        int iSrcDaps = 0, iDstDaps = 0;

        // following Forwarding's same routing routine, retrieve both in-bound and out-bound routes for
        // all clusters.
        while ((iSrcDaps < srcDaps.length) && (iDstDaps < dstDaps.length)) {
            SwitchPort srcDap = srcDaps[iSrcDaps];
            SwitchPort dstDap = dstDaps[iDstDaps];
            Long srcCluster = 
                    topology.getL2DomainId(srcDap.getSwitchDPID());
            Long dstCluster = 
                    topology.getL2DomainId(dstDap.getSwitchDPID());

            int srcVsDest = srcCluster.compareTo(dstCluster);
            if (srcVsDest == 0) {
                if (!srcDap.equals(dstDap) && 
                        (srcCluster != null) && 
                        (dstCluster != null)) {
                    Route routeIn = 
                            routingEngine.getRoute(srcDap.getSwitchDPID(),
                                                   (short)srcDap.getPort(),
                                                   dstDap.getSwitchDPID(),
                                                   (short)dstDap.getPort(), 0);
                    Route routeOut = 
                            routingEngine.getRoute(dstDap.getSwitchDPID(),
                                                   (short)dstDap.getPort(),
                                                   srcDap.getSwitchDPID(),
                                                   (short)srcDap.getPort(), 0);

                    // use static flow entry pusher to push flow mod along in and out path
                    // in: match src client (ip, port), rewrite dest from vip ip/port to member ip/port, forward
                    // out: match dest client (ip, port), rewrite src from member ip/port to vip ip/port, forward
                    
                    if (routeIn != null) {
                        pushStaticVipRoute(true, routeIn, pinfo, bkserver,dstDevice.getMACAddressString(), sw.getId());
                    } else {
                        log.info("routIn is null");
                    }
                    
                    if (routeOut != null) {
                        pushStaticVipRoute(false, routeOut, pinfo, bkserver,vipMac, sw.getId());
                    } else {
                        log.info("routeOut in null");
                    }

                }
                iSrcDaps++;
                iDstDaps++;
            } else if (srcVsDest < 0) {
                iSrcDaps++;
            } else {
                iDstDaps++;
            }
        }
        return;
    }
    
    /**
     * used to push given route using static flow entry pusher
     * @param boolean inBound
     * @param Route route
     * @param IPClient client
     * @param LBMember member
     * @param long pinSwitch
     */
    public void pushStaticVipRoute(boolean inBound, Route route, PacketInfo pinfo, String bkserver, String mac, long pinSwitch) {
        List<NodePortTuple> path = route.getPath();
        if (path.size()>0) {
           for (int i = 0; i < path.size(); i+=2) {
               
               long sw = path.get(i).getNodeId();
               String swString = HexString.toHexString(path.get(i).getNodeId());
               String entryName;
               String matchString = null;
               String actionString = null;
               
               OFFlowMod fm = (OFFlowMod) floodlightProvider.getOFMessageFactory()
                       .getMessage(OFType.FLOW_MOD);

               fm.setIdleTimeout((short) 3);   // same as ODL version
               fm.setHardTimeout((short) 0);   // 
               fm.setBufferId(OFPacketOut.BUFFER_ID_NONE);
               fm.setCommand((short) 0);
               fm.setFlags((short) 0);
               fm.setOutPort(OFPort.OFPP_NONE.getValue());
               fm.setCookie((long) 0);  
               fm.setPriority(U16.t(LB_IPSWITCH_PRIORITY));
               
               if (inBound) {
                   entryName = "inbound-vip-"+ pinfo.getDip()+"-vport-"+pinfo.getDport()+"-client-"+pinfo.getSip()+"-cport-"+pinfo.getSport()
                           +"-srcswitch-"+path.get(0).getNodeId()+"-sw-"+sw;
                   matchString = "nw_src="+IPv4.fromIPv4Address(pinfo.getSip())+","
                   //matchString = "nw_src=10.1.2.6/24,"
                               + "nw_proto="+String.valueOf(pinfo.getProtocol())+","
                               + "tp_src="+String.valueOf(pinfo.getSport() & 0xffff)+","
                               + "nw_dst="+IPv4.fromIPv4Address(pinfo.getDip())+","
                               + "tp_dst="+String.valueOf(pinfo.getDport() & 0xffff)+","
                               //+ "dl_type="+LB_ETHER_TYPE
                               + "dl_type="+LB_ETHER_TYPE+","
                               + "in_port="+String.valueOf(path.get(i).getPortId());

                   if (sw == pinSwitch) {
                       actionString = "set-dst-ip="+bkserver.split(":")[0]+"," 
                    		   + "set-dst-port="+bkserver.split(":")[1]+","
                                + "set-dst-mac="+mac+","
                                + "output="+path.get(i+1).getPortId();
                   } else {
                       actionString =
                               "output="+path.get(i+1).getPortId();
                   }
                   
               } else {
                   entryName = "outbound-vip-"+ pinfo.getDip()+"-vport-"+pinfo.getDport()+"-client-"+pinfo.getSip()+"-port-"+pinfo.getSport()
                           +"-srcswitch-"+path.get(0).getNodeId()+"-sw-"+sw;
                   matchString = "nw_dst="+IPv4.fromIPv4Address(pinfo.getSip())+","
                               + "nw_proto="+String.valueOf(pinfo.getProtocol())+","
                               + "tp_dst="+String.valueOf(pinfo.getSport() & 0xffff)+","
                               + "nw_src="+bkserver.split(":")[0]+","
                               + "dl_type="+LB_ETHER_TYPE+","
                               + "in_port="+String.valueOf(path.get(i).getPortId());

                   if (sw == pinSwitch) {
                       actionString = "set-src-ip="+IPv4.fromIPv4Address(pinfo.getDip())+","
                               //+ "set-src-mac="+vips.get(member.vipId).proxyMac.toString()+","
                               + "set-src-port="+String.valueOf(pinfo.getDport() & 0xffff)+","
                               + "set-src-mac="+mac+","
                               + "output="+path.get(i+1).getPortId();
                   } else {
                       actionString = "output="+path.get(i+1).getPortId();
                   }
                   
               }
               
               parseActionString(fm, actionString, log);

               fm.setPriority(U16.t(LB_IPSWITCH_PRIORITY));

               OFMatch ofMatch = new OFMatch();
              // ofMatch.setWildcards(Wildcards.FULL.matchOn(Wildcards.Flag.NW_SRC).withNwSrcMask(24));
               try {
                   ofMatch.fromString(matchString);
               } catch (IllegalArgumentException e) {
                   log.debug("ignoring flow entry {} on switch {} with illegal OFMatch() key: "
                                     + matchString, entryName, swString);
               }
               //ofMatch.setWildcards(Wildcards.FULL.matchOn(Wildcards.Flag.NW_SRC).withNwSrcMask(24));
               
               fm.setMatch(ofMatch);
               
               sfp.addFlow(entryName, fm, swString);
               //log.info("sw:{} match:{} action:{} priority:{}",
               //        new Object[]{ swString, ofMatch.toString(), actionString,
               //            U16.t(LB_IPSWITCH_PRIORITY)});

           }
        }
        return;
    }


    @Override
    public Collection<Class<? extends IFloodlightService>>
            getModuleServices() {
        Collection<Class<? extends IFloodlightService>> l = 
                new ArrayList<Class<? extends IFloodlightService>>();
        l.add(ILoadBalancerService.class);
        return l;
   }

    @Override
    public Map<Class<? extends IFloodlightService>, IFloodlightService>
            getServiceImpls() {
        Map<Class<? extends IFloodlightService>, IFloodlightService> m = 
                new HashMap<Class<? extends IFloodlightService>,
                    IFloodlightService>();
        m.put(ILoadBalancerService.class, this);
        return m;
    }

    @Override
    public Collection<Class<? extends IFloodlightService>>
            getModuleDependencies() {
        Collection<Class<? extends IFloodlightService>> l = 
                new ArrayList<Class<? extends IFloodlightService>>();
        l.add(IFloodlightProviderService.class);
        l.add(IRestApiService.class);
        l.add(ICounterStoreService.class);
        l.add(IDeviceService.class);
        l.add(ITopologyService.class);
        l.add(IRoutingService.class);
        l.add(IStaticFlowEntryPusherService.class);

        return l;
    }

    @Override
    public void init(FloodlightModuleContext context)
                                                 throws FloodlightModuleException {
        floodlightProvider = context.getServiceImpl(IFloodlightProviderService.class);
        restApi = context.getServiceImpl(IRestApiService.class);
        counterStore = context.getServiceImpl(ICounterStoreService.class);
        deviceManager = context.getServiceImpl(IDeviceService.class);
        routingEngine = context.getServiceImpl(IRoutingService.class);
        topology = context.getServiceImpl(ITopologyService.class);
        sfp = context.getServiceImpl(IStaticFlowEntryPusherService.class);
        storage=context.getServiceImpl(IStorageSourceService.class);
        
        messageDamper = new OFMessageDamper(OFMESSAGE_DAMPER_CAPACITY, 
                                            EnumSet.of(OFType.FLOW_MOD),
                                            OFMESSAGE_DAMPER_TIMEOUT);

    }

    @Override
    public void startUp(FloodlightModuleContext context) {
        floodlightProvider.addOFMessageListener(OFType.PACKET_IN, this);
        restApi.addRestletRoutable(new LoadBalancerWebRoutable());

        LBPool pool1 = new LBPool("10", "10.1.2.100", 0,1, (short) 8000,1);
        LBPool pool2 = new LBPool("11", "10.1.2.200", 0,1, (short) 8000,1);
        LBBkServers bks1 = new LBBkServers("20", "10.1.2.11",(short) 8000,1,1,"",null);
        LBBkServers bks2 = new LBBkServers("21", "10.1.2.12",(short) 8000,1,1,"",null);
        createPool(pool1);
        createPool(pool2);
        createBkServer(bks1);
        createBkServer(bks2);
        addBkstoPool(pool1.getId(), bks1.getId());
        addBkstoPool(pool2.getId(), bks2.getId());
    }
    // Utilities borrowed from StaticFlowEntries
    
    public static class SubActionStruct {
        OFAction action;
        int      len;
    }
    
    /**
     * Parses OFFlowMod actions from strings.
     * @param flowMod The OFFlowMod to set the actions for
     * @param actionstr The string containing all the actions
     * @param log A logger to log for errors.
     */
    public static void parseActionString(OFFlowMod flowMod, String actionstr, Logger log) {
        List<OFAction> actions = new LinkedList<OFAction>();
        int actionsLength = 0;
        if (actionstr != null) {
            actionstr = actionstr.toLowerCase();
            for (String subaction : actionstr.split(",")) {
                String action = subaction.split("[=:]")[0];
                SubActionStruct subaction_struct = null;
                
                if (action.equals("output")) {
                    subaction_struct = decode_output(subaction, log);
                }
                else if (action.equals("enqueue")) {
                    subaction_struct = decode_enqueue(subaction, log);
                }
                else if (action.equals("strip-vlan")) {
                    subaction_struct = decode_strip_vlan(subaction, log);
                }
                else if (action.equals("set-vlan-id")) {
                    subaction_struct = decode_set_vlan_id(subaction, log);
                }
                else if (action.equals("set-vlan-priority")) {
                    subaction_struct = decode_set_vlan_priority(subaction, log);
                }
                else if (action.equals("set-src-mac")) {
                    subaction_struct = decode_set_src_mac(subaction, log);
                }
                else if (action.equals("set-dst-mac")) {
                    subaction_struct = decode_set_dst_mac(subaction, log);
                }
                else if (action.equals("set-tos-bits")) {
                    subaction_struct = decode_set_tos_bits(subaction, log);
                }
                else if (action.equals("set-src-ip")) {
                    subaction_struct = decode_set_src_ip(subaction, log);
                }
                else if (action.equals("set-dst-ip")) {
                    subaction_struct = decode_set_dst_ip(subaction, log);
                }
                else if (action.equals("set-src-port")) {
                    subaction_struct = decode_set_src_port(subaction, log);
                }
                else if (action.equals("set-dst-port")) {
                    subaction_struct = decode_set_dst_port(subaction, log);
                }
                else {
                    log.error("Unexpected action '{}', '{}'", action, subaction);
                }
                
                if (subaction_struct != null) {
                    actions.add(subaction_struct.action);
                    actionsLength += subaction_struct.len;
                }
            }
        }
        log.debug("action {}", actions);
        
        flowMod.setActions(actions);
        flowMod.setLengthU(OFFlowMod.MINIMUM_LENGTH + actionsLength);
    } 
    
    private static SubActionStruct decode_output(String subaction, Logger log) {
        SubActionStruct sa = null;
        Matcher n;
        
        n = Pattern.compile("output=(?:((?:0x)?\\d+)|(all)|(controller)|(local)|(ingress-port)|(normal)|(flood))").matcher(subaction);
        if (n.matches()) {
            OFActionOutput action = new OFActionOutput();
            action.setMaxLength(Short.MAX_VALUE);
            short port = OFPort.OFPP_NONE.getValue();
            if (n.group(1) != null) {
                try {
                    port = get_short(n.group(1));
                }
                catch (NumberFormatException e) {
                    log.debug("Invalid port in: '{}' (error ignored)", subaction);
                    return null;
                }
            }
            else if (n.group(2) != null)
                port = OFPort.OFPP_ALL.getValue();
            else if (n.group(3) != null)
                port = OFPort.OFPP_CONTROLLER.getValue();
            else if (n.group(4) != null)
                port = OFPort.OFPP_LOCAL.getValue();
            else if (n.group(5) != null)
                port = OFPort.OFPP_IN_PORT.getValue();
            else if (n.group(6) != null)
                port = OFPort.OFPP_NORMAL.getValue();
            else if (n.group(7) != null)
                port = OFPort.OFPP_FLOOD.getValue();
            action.setPort(port);
            log.debug("action {}", action);
            
            sa = new SubActionStruct();
            sa.action = action;
            sa.len = OFActionOutput.MINIMUM_LENGTH;
        }
        else {
            log.error("Invalid subaction: '{}'", subaction);
            return null;
        }
        
        return sa;
    }
    
    private static SubActionStruct decode_enqueue(String subaction, Logger log) {
        SubActionStruct sa = null;
        Matcher n;
        
        n = Pattern.compile("enqueue=(?:((?:0x)?\\d+)\\:((?:0x)?\\d+))").matcher(subaction);
        if (n.matches()) {
            short portnum = 0;
            if (n.group(1) != null) {
                try {
                    portnum = get_short(n.group(1));
                }
                catch (NumberFormatException e) {
                    log.debug("Invalid port-num in: '{}' (error ignored)", subaction);
                    return null;
                }
            }

            int queueid = 0;
            if (n.group(2) != null) {
                try {
                    queueid = get_int(n.group(2));
                }
                catch (NumberFormatException e) {
                    log.debug("Invalid queue-id in: '{}' (error ignored)", subaction);
                    return null;
               }
            }
            
            OFActionEnqueue action = new OFActionEnqueue();
            action.setPort(portnum);
            action.setQueueId(queueid);
            log.debug("action {}", action);
            
            sa = new SubActionStruct();
            sa.action = action;
            sa.len = OFActionEnqueue.MINIMUM_LENGTH;
        }
        else {
            log.debug("Invalid action: '{}'", subaction);
            return null;
        }
        
        return sa;
    }
    
    private static SubActionStruct decode_strip_vlan(String subaction, Logger log) {
        SubActionStruct sa = null;
        Matcher n = Pattern.compile("strip-vlan").matcher(subaction);
        
        if (n.matches()) {
            OFActionStripVirtualLan action = new OFActionStripVirtualLan();
            log.debug("action {}", action);
            
            sa = new SubActionStruct();
            sa.action = action;
            sa.len = OFActionStripVirtualLan.MINIMUM_LENGTH;
        }
        else {
            log.debug("Invalid action: '{}'", subaction);
            return null;
        }

        return sa;
    }
    
    private static SubActionStruct decode_set_vlan_id(String subaction, Logger log) {
        SubActionStruct sa = null;
        Matcher n = Pattern.compile("set-vlan-id=((?:0x)?\\d+)").matcher(subaction);
        
        if (n.matches()) {            
            if (n.group(1) != null) {
                try {
                    short vlanid = get_short(n.group(1));
                    OFActionVirtualLanIdentifier action = new OFActionVirtualLanIdentifier();
                    action.setVirtualLanIdentifier(vlanid);
                    log.debug("  action {}", action);

                    sa = new SubActionStruct();
                    sa.action = action;
                    sa.len = OFActionVirtualLanIdentifier.MINIMUM_LENGTH;
                }
                catch (NumberFormatException e) {
                    log.debug("Invalid VLAN in: {} (error ignored)", subaction);
                    return null;
                }
            }          
        }
        else {
            log.debug("Invalid action: '{}'", subaction);
            return null;
        }

        return sa;
    }
    
    private static SubActionStruct decode_set_vlan_priority(String subaction, Logger log) {
        SubActionStruct sa = null;
        Matcher n = Pattern.compile("set-vlan-priority=((?:0x)?\\d+)").matcher(subaction); 
        
        if (n.matches()) {
            if (n.group(1) != null) {
                try {
                    byte prior = get_byte(n.group(1));
                    OFActionVirtualLanPriorityCodePoint action = new OFActionVirtualLanPriorityCodePoint();
                    action.setVirtualLanPriorityCodePoint(prior);
                    log.debug("  action {}", action);
                    
                    sa = new SubActionStruct();
                    sa.action = action;
                    sa.len = OFActionVirtualLanPriorityCodePoint.MINIMUM_LENGTH;
                }
                catch (NumberFormatException e) {
                    log.debug("Invalid VLAN priority in: {} (error ignored)", subaction);
                    return null;
                }
            }
        }
        else {
            log.debug("Invalid action: '{}'", subaction);
            return null;
        }

        return sa;
    }
    
    private static SubActionStruct decode_set_src_mac(String subaction, Logger log) {
        SubActionStruct sa = null;
        Matcher n = Pattern.compile("set-src-mac=(?:(\\p{XDigit}+)\\:(\\p{XDigit}+)\\:(\\p{XDigit}+)\\:(\\p{XDigit}+)\\:(\\p{XDigit}+)\\:(\\p{XDigit}+))").matcher(subaction); 

        if (n.matches()) {
            byte[] macaddr = get_mac_addr(n, subaction, log);
            if (macaddr != null) {
                OFActionDataLayerSource action = new OFActionDataLayerSource();
                action.setDataLayerAddress(macaddr);
                log.debug("action {}", action);

                sa = new SubActionStruct();
                sa.action = action;
                sa.len = OFActionDataLayerSource.MINIMUM_LENGTH;
            }
        }
        else {
            log.debug("Invalid action: '{}'", subaction);
            return null;
        }

        return sa;
    }

    private static SubActionStruct decode_set_dst_mac(String subaction, Logger log) {
        SubActionStruct sa = null;
        Matcher n = Pattern.compile("set-dst-mac=(?:(\\p{XDigit}+)\\:(\\p{XDigit}+)\\:(\\p{XDigit}+)\\:(\\p{XDigit}+)\\:(\\p{XDigit}+)\\:(\\p{XDigit}+))").matcher(subaction);
        
        if (n.matches()) {
            byte[] macaddr = get_mac_addr(n, subaction, log);
            if (macaddr != null) {
                OFActionDataLayerDestination action = new OFActionDataLayerDestination();
                action.setDataLayerAddress(macaddr);
                log.debug("  action {}", action);
                
                sa = new SubActionStruct();
                sa.action = action;
                sa.len = OFActionDataLayerDestination.MINIMUM_LENGTH;
            }
        }
        else {
            log.debug("Invalid action: '{}'", subaction);
            return null;
        }

        return sa;
    }
    
    private static SubActionStruct decode_set_tos_bits(String subaction, Logger log) {
        SubActionStruct sa = null;
        Matcher n = Pattern.compile("set-tos-bits=((?:0x)?\\d+)").matcher(subaction); 

        if (n.matches()) {
            if (n.group(1) != null) {
                try {
                    byte tosbits = get_byte(n.group(1));
                    OFActionNetworkTypeOfService action = new OFActionNetworkTypeOfService();
                    action.setNetworkTypeOfService(tosbits);
                    log.debug("  action {}", action);
                    
                    sa = new SubActionStruct();
                    sa.action = action;
                    sa.len = OFActionNetworkTypeOfService.MINIMUM_LENGTH;
                }
                catch (NumberFormatException e) {
                    log.debug("Invalid dst-port in: {} (error ignored)", subaction);
                    return null;
                }
            }
        }
        else {
            log.debug("Invalid action: '{}'", subaction);
            return null;
        }

        return sa;
    }
    
    private static SubActionStruct decode_set_src_ip(String subaction, Logger log) {
        SubActionStruct sa = null;
        Matcher n = Pattern.compile("set-src-ip=(?:(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+))").matcher(subaction);

        if (n.matches()) {
            int ipaddr = get_ip_addr(n, subaction, log);
            OFActionNetworkLayerSource action = new OFActionNetworkLayerSource();
            action.setNetworkAddress(ipaddr);
            log.debug("  action {}", action);

            sa = new SubActionStruct();
            sa.action = action;
            sa.len = OFActionNetworkLayerSource.MINIMUM_LENGTH;
        }
        else {
            log.debug("Invalid action: '{}'", subaction);
            return null;
        }

        return sa;
    }

    private static SubActionStruct decode_set_dst_ip(String subaction, Logger log) {
        SubActionStruct sa = null;
        Matcher n = Pattern.compile("set-dst-ip=(?:(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+))").matcher(subaction);

        if (n.matches()) {
            int ipaddr = get_ip_addr(n, subaction, log);
            OFActionNetworkLayerDestination action = new OFActionNetworkLayerDestination();
            action.setNetworkAddress(ipaddr);
            log.debug("action {}", action);
 
            sa = new SubActionStruct();
            sa.action = action;
            sa.len = OFActionNetworkLayerDestination.MINIMUM_LENGTH;
        }
        else {
            log.debug("Invalid action: '{}'", subaction);
            return null;
        }

        return sa;
    }

    private static SubActionStruct decode_set_src_port(String subaction, Logger log) {
        SubActionStruct sa = null;
        Matcher n = Pattern.compile("set-src-port=((?:0x)?\\d+)").matcher(subaction); 

        if (n.matches()) {
            if (n.group(1) != null) {
                try {
                    short portnum = get_short(n.group(1));
                    OFActionTransportLayerSource action = new OFActionTransportLayerSource();
                    action.setTransportPort(portnum);
                    log.debug("action {}", action);
                    
                    sa = new SubActionStruct();
                    sa.action = action;
                    sa.len = OFActionTransportLayerSource.MINIMUM_LENGTH;;
                }
                catch (NumberFormatException e) {
                    log.debug("Invalid src-port in: {} (error ignored)", subaction);
                    return null;
                }
            }
        }
        else {
            log.debug("Invalid action: '{}'", subaction);
            return null;
        }

        return sa;
    }

    private static SubActionStruct decode_set_dst_port(String subaction, Logger log) {
        SubActionStruct sa = null;
        Matcher n = Pattern.compile("set-dst-port=((?:0x)?\\d+)").matcher(subaction);

        if (n.matches()) {
            if (n.group(1) != null) {
                try {
                    short portnum = get_short(n.group(1));
                    OFActionTransportLayerDestination action = new OFActionTransportLayerDestination();
                    action.setTransportPort(portnum);
                    log.debug("action {}", action);
                    
                    sa = new SubActionStruct();
                    sa.action = action;
                    sa.len = OFActionTransportLayerDestination.MINIMUM_LENGTH;;
                }
                catch (NumberFormatException e) {
                    log.debug("Invalid dst-port in: {} (error ignored)", subaction);
                    return null;
                }
            }
        }
        else {
            log.debug("Invalid action: '{}'", subaction);
            return null;
        }

        return sa;
    }
    
    private static byte[] get_mac_addr(Matcher n, String subaction, Logger log) {
        byte[] macaddr = new byte[6];
        
        for (int i=0; i<6; i++) {
            if (n.group(i+1) != null) {
                try {
                    macaddr[i] = get_byte("0x" + n.group(i+1));
                }
                catch (NumberFormatException e) {
                    log.debug("Invalid src-mac in: '{}' (error ignored)", subaction);
                    return null;
                }
            }
            else { 
                log.debug("Invalid src-mac in: '{}' (null, error ignored)", subaction);
                return null;
            }
        }
        
        return macaddr;
    }
    
    private static int get_ip_addr(Matcher n, String subaction, Logger log) {
        int ipaddr = 0;

        for (int i=0; i<4; i++) {
            if (n.group(i+1) != null) {
                try {
                    ipaddr = ipaddr<<8;
                    ipaddr = ipaddr | get_int(n.group(i+1));
                }
                catch (NumberFormatException e) {
                    log.debug("Invalid src-ip in: '{}' (error ignored)", subaction);
                    return 0;
                }
            }
            else {
                log.debug("Invalid src-ip in: '{}' (null, error ignored)", subaction);
                return 0;
            }
        }
        
        return ipaddr;
    }
    
    // Parse int as decimal, hex (start with 0x or #) or octal (starts with 0)
    private static int get_int(String str) {
        return Integer.decode(str);
    }
   
    // Parse short as decimal, hex (start with 0x or #) or octal (starts with 0)
    private static short get_short(String str) {
        return (short)(int)Integer.decode(str);
    }
   
    // Parse byte as decimal, hex (start with 0x or #) or octal (starts with 0)
    private static byte get_byte(String str) {
        return Integer.decode(str).byteValue();
    }
    
    //Interfaces in ILoadBalancerService/////////////////////////
    
  //Get pool list. return all pools when pid=0
    @Override
    public Collection<LBPool> getPools(String poolId)
    {
    	HashMap<String, LBPool> result=new HashMap<String, LBPool>();
    	if(poolId.equals("0"))
    	{
    		result=this.pools;
    	}
    	else
    	{
	    	LBPool pool=null;
	    	Iterator iter = pools.entrySet().iterator();
	    	while (iter.hasNext()) {
	    		Map.Entry entry = (Map.Entry) iter.next();
	    		Object key = entry.getKey();
	    		pool = (LBPool)(entry.getValue());
	    		if(pool.getId().equals(poolId))
	    		{
	    			result.put((String) key, pool);
	    			break;
	    		}
	    	}
    	}
    	return result.values();
    }

    //Create new pool
    @Override
    public int createPool(LBPool pool)
    {
    	if(pool.getVip().equals("-999"))
    		return -100;
    	if(pool.getPort()==-999)
    		return -101;
    	if(pool.getLbMethod()==-999)
    		return -104;
    	if(pool.getIsactive()==-999)
    		return -103;
    	if(pools.containsKey(IPv4.toIPv4Address(pool.getVip())+":"+pool.getPort()))
    		return -1;
    	//pool.setId(String.valueOf((int) (Math.random()*10000)));
    	pools.put(IPv4.toIPv4Address(pool.getVip())+":"+pool.getPort(), pool);
    	vipArp.put(IPv4.toIPv4Address(pool.getVip()), pool.getVipMac());
    	return 1;
    }
    
    //Update pool info
    @Override
    public int updatePool(LBPool pool)
    {
    	if(pool.getVip().equals("-999"))
    		return -100;
    	if(pool.getPort()==-999)
    		return -101;
    	if(pool.getLbMethod()==-999)
    		return -104;
    	if(pool.getIsactive()==-999)
    		return -103;
    	if(pool.getProtocol()==-999)
    		return -105;
    	
    	LBPool cpool=null;
    	int t=0;
    	Iterator iter =  pools.entrySet().iterator();
    	while (iter.hasNext()) {
    		Map.Entry entryid = (Map.Entry) iter.next();
    		Object keyid = entryid.getKey();
    		cpool = (LBPool)(entryid.getValue());
    		if(cpool.getId().equals(pool.getId()))
    		{
    			t=1;
    			break;
    		}
    	}
    	if(t==0)
    		return -3;
    		
    	if(pools.containsKey(IPv4.toIPv4Address(pool.getVip())+":"+pool.getPort()) && pools.get(IPv4.toIPv4Address(pool.getVip())+":"+pool.getPort()).getId().equals(pool.getId())!=true)
    		return -2;
    	while (iter.hasNext()) {
    		Map.Entry entry = (Map.Entry) iter.next();
    		Object key = entry.getKey();
    		cpool = (LBPool)(entry.getValue());
    		if(cpool.getId().equals(pool.getId()))
    		{
    			vipArp.remove(cpool.getVip());
    			if(pool.getName()!=null &&pool.getName()!="")
    				cpool.setName(pool.getName());
    			if(pool.getVip()!=null && pool.getVip()!="" &&!pool.getVip().equals("-999"))
    				cpool.setVip(pool.getVip());
    			if(pool.getLbMethod()!=-999)
    				cpool.setLbMethod(pool.getLbMethod());
    			if(pool.getIsactive()!=-999)
    				cpool.setIsactive(pool.getIsactive());
    			if(pool.getPort()!=-999)
    				cpool.setPort(pool.getPort());
    			if(pool.getProtocol()!=-999)
    				cpool.setProtocol(pool.getPort());
    			pools.remove(key);
    	    	pools.put(IPv4.toIPv4Address(cpool.getVip())+":"+cpool.getPort(), cpool);
    	    	vipArp.put(IPv4.toIPv4Address(cpool.getVip()), cpool.getVipMac());
    	    	return 1;
    		}
    	}
    	return -3;
    }
    
    //Delete pool
    @Override
    public int deletePool(String poolId)
    {
    	LBPool cpool=null;
    	Iterator iter =  pools.entrySet().iterator();
    	while (iter.hasNext()) {
    		Map.Entry entry = (Map.Entry) iter.next();
    		Object key = entry.getKey();
    		cpool = (LBPool)(entry.getValue());
    		if(cpool.getId().equals(poolId))
    		{
    			pools.remove(key);
    			vipArp.remove(IPv4.toIPv4Address(cpool.getVip()));
    	    	return 1;
    		}
    	}
    	return -3;
    }

    //////////////////////
    //Get back server list
    @Override
    public Collection<LBBkServers> getBkServers(String bksId)
    {
    	HashMap<String,LBBkServers> result=new HashMap<String,LBBkServers>();
    	if(bksId.equals("0"))
    	{
    		result=this.bkservers;
    	}
    	else
    	{
	    	LBBkServers bks=null;
	    	Iterator iter = bkservers.entrySet().iterator();
	    	while (iter.hasNext()) {
	    		Map.Entry entry = (Map.Entry) iter.next();
	    		Object key = entry.getKey();
	    		bks = (LBBkServers)(entry.getValue());
	    		if(bks.getId().equals(bksId))
	    		{
	    			result.put((String) key, bks);
	    			break;
	    		}
	    	}
    	}
    	return result.values();
    }
    
    //Create new back server
    @Override
    public int createBkServer(LBBkServers bks)
    {
    	if(bks.getIp().equals("-999"))
    		return -100;
    	if(bks.getPort()==-999)
    		return -101;
    	if(bks.getPriority()==-999)
    		return -102;
    	if(bks.getIsactive()==-999)
    		return -103;
    	Iterator iter = this.bkservers.entrySet().iterator();
    	LBBkServers tbks=null;
    	while (iter.hasNext()) {
    		Map.Entry entry = (Map.Entry) iter.next();
    		Object key = entry.getKey();
    		tbks = (LBBkServers)(entry.getValue());
    		if(tbks.hashCode()==bks.hashCode())
    		{
    			return -2;
    		}
    	}
    	this.bkservers.put(bks.getId(),bks);
    	return 1;
    }
    
    //Update back server
    @Override
    public int updateBkServer(LBBkServers bks)
    {
		//verify the param
		if(bks.getIp().equals("-999"))
    		return -100;
    	if(bks.getPort()==-999)
    		return -101;
    	if(bks.getPriority()==-999)
    		return -102;
    	if(bks.getIsactive()==-999)
    		return -103;
    	LBBkServers tbks=null;
    	String bksid="";
    	if(!this.bkservers.containsKey(bks.getId()))
    		return -4;
    	Iterator iter = this.bkservers.entrySet().iterator();
    	while (iter.hasNext()) {
    		Map.Entry entry = (Map.Entry) iter.next();
    		Object key = entry.getKey();
    		LBBkServers tbksc = (LBBkServers)(entry.getValue());
    		if(tbksc.getId().equals(bks.getId()))
    		{
    			
    			//update var bkservers
    			bksid=tbksc.getId();
    			if(bks.getName()==null || bks.getName()=="")
    				bks.setName(tbksc.getName());
    			if(bks.getIp()==null || bks.getIp()==""|| bks.getIp().equals("-999"))
    				bks.setIp(tbksc.getIp());
    			if(bks.getIsactive()==-999)
    				bks.setIsactive(tbksc.getIsactive());
    			if(bks.getPort()==-999)
    				bks.setPort(tbksc.getPort());
    			if(bks.getPriority()==-999)
    				bks.setPriority(tbksc.getPriority());
    			tbks=bks;
    			//this.bkservers.put(tbks.getId(), tbks);
    			//break;
    		}
    		else
    		{
    			if(tbksc.getIp().equals(bks.getIp())&&tbksc.getPort()==bks.getPort())
    				return -2;
    		}
    	}
    	
    	this.bkservers.put(tbks.getId(), tbks);
    	//update all pools
    	iter = pools.entrySet().iterator();
    	LBPool pool=null;
    	while (iter.hasNext()) {
    		Map.Entry entry = (Map.Entry) iter.next();
    		Object key = entry.getKey();
    		pool = (LBPool)(entry.getValue());
    		pool.updateBkServer(tbks);
    		//update pools hash map
    		this.pools.put((String) key, pool);
    	}
    	return 1;
    }
    
    //Delete back server
    @Override
    public int deleteBkServer(String bksId)
    {
    	if(!this.bkservers.containsKey(bksId))
    		return -4;
    	LBBkServers removebks=this.bkservers.get(bksId);
    	LBPool pool=null;
    	//delete from var bkservers
    	this.bkservers.remove(bksId);
    	
    	//delete from all pools
    	Iterator iter = pools.entrySet().iterator();
    	while (iter.hasNext()) {
    		Map.Entry entry = (Map.Entry) iter.next();
    		Object key = entry.getKey();
    		pool = (LBPool)(entry.getValue());
    		pool.deleteBkServer(bksId);
    		//update pools hash map
    		this.pools.put((String) key, pool);
    	}
    	
    	//delete from all vlans
    	Iterator viter = vlans.entrySet().iterator();
    	LBVlan vlan=null;
    	while (viter.hasNext()) {
    		Map.Entry ventry = (Map.Entry) viter.next();
    		Object vkey = ventry.getKey();
    		vlan = (LBVlan)(ventry.getValue());
    		vlan.delBkids(bksId);
    		//update pools hash map
    		this.vlans.put((String) vkey, vlan);
    	}
    	return 1;
    }
    
    ///////////////////////////////////////
  //Create new vlan
  	public int createVlan(LBVlan vlan)
  	{
  		if(vlan.getIsactive()==-999)
  			return -103;
    	vlan.setId(String.valueOf((int) (Math.random()*10000)));
    	this.vlans.put(vlan.getId(), vlan);
    	return 1;
  	}
  	
  	//Update vlan info
  	public int updateVlan(LBVlan vlan)
  	{
  		if(vlan.getIsactive()==-999)
  			return -103;
  		LBVlan tvlan=this.vlans.get(vlan.getId());
  		if(tvlan==null)
  			return -5;
  		if(vlan.getName()!=null && vlan.getName()!="")
			tvlan.setName(vlan.getName());
  		if(vlan.getIsactive()!=-999)
  			tvlan.setIsactive(vlan.getIsactive());
    	
  		this.vlans.put(tvlan.getId(), tvlan);
  		
  		//change all back servers status
    	ArrayList<String> bkids=tvlan.getBksids();
    	for(String id : bkids)
    	{
    		LBBkServers bks=this.bkservers.get(id);
    		if(bks!=null)
    		{
    			bks.setIsactive(vlan.getIsactive());
    			this.updateBkServer(bks);
    		}
    	}
    	return 1;
  	}
  	
  	//Delete vlan
  	public int deleteVlan(String vlanId)
  	{
  		if(!this.vlans.containsKey(vlanId))
  			return -5;
  		this.vlans.remove(vlanId);
  		return 1;
  	}
   //Get vlan list. return all vlans when vlanid=0
    @Override
    public Collection<LBVlan> getVlans(String vlanId)
    {
    	HashMap<String, LBVlan> result=new HashMap<String, LBVlan>();
    	if(vlanId.equals("0"))
    	{
    		result=this.vlans;
    	}
    	else
    	{
	    	if(this.vlans.containsKey(vlanId))
	    		result.put(vlanId, this.vlans.get(vlanId));
    	}
    	return result.values();
    }
    
    ////////////////////////////////////
    //add back server to pool
    public int addBkstoPool(String poolId, String bksId)
    {
    	if(!this.bkservers.containsKey(bksId))
    		return -4;
    	LBBkServers bks=this.bkservers.get(bksId);
        if (bks.getPoolid() != null) {
            return -8;
        }
    	LBPool pool = null;
    	Iterator iter = pools.entrySet().iterator();
    	while (iter.hasNext()) {
    		Map.Entry entry = (Map.Entry) iter.next();
    		Object key = entry.getKey();
    		pool = (LBPool)(entry.getValue());
    		if(pool.getId().equals(poolId))
    		{
    			ArrayList<LBBkServers> t = pool.getServers();
    			for(int i=0;i<t.size();i++)
    			{
    				if(t.get(i).getId().equals(bksId))
    					return -7;
    			}
    			pool.addBkServer(bks);
    			pools.put((String)key, pool);
                bks.setPoolid(pool.getId());
    			return 1;
    		}
    	}
    	return -3;
    }
    
    //delete back server from pool
    public int delBksfromPool(String poolId, String bksId)
    {
    	if(!this.bkservers.containsKey(bksId))
    		return -4;
    	LBBkServers bks=this.bkservers.get(bksId);
    	if(!bks.getPoolid().equals(poolId))
    		return -10;
    	LBPool pool=null;
    	Iterator iter = pools.entrySet().iterator();
    	while (iter.hasNext()) {
    		Map.Entry entry = (Map.Entry) iter.next();
    		Object key = entry.getKey();
    		pool = (LBPool)(entry.getValue());
    		if(pool.getId().equals(poolId))
    		{
    			
    			pool.deleteBkServer(bksId);
    			
        		//update pools hash map
        		this.pools.put((String) key, pool);
                bks.setPoolid(null);
    			return 1;
    		}
    	}
    	return -3;
    }  
    
    //add back server to vlan
    public int addBkstovlan(String vlanId, String bksId)
    {
    	int code=0;
    	if(!this.vlans.containsKey(vlanId))
    		return -5;
    	if(!this.bkservers.containsKey(bksId))
    		return -4;
    	LBVlan vlan=this.vlans.get(vlanId);
    	code=vlan.addBksids(bksId);
    	this.vlans.put(vlanId, vlan);
       // this.bkservers.get(bksId).setVlanid(vlanId);
    	return code;
    }
    
    //delete back server from vlan
    public int delBksfromVlan(String vlanId, String bksId)
    {
    	if(!this.vlans.containsKey(vlanId))
    		return -5;
    	if(!this.bkservers.containsKey(bksId))
    		return -4;
    	LBVlan vlan=this.vlans.get(vlanId);
    	if(vlan.delBkids(bksId)==-1)
    		return -9;
    	this.vlans.put(vlanId, vlan);
    	return 1;
    }
    
    public int routeAddr(String dpid)
    {
    	Map<String, OFFlowMod> mf=sfp.getFlows(dpid);
    	FlowAggr fa=new FlowAggr();
    	fa.aggr(dpid,mf,sfp,pools);
    	return 0;
    }
}

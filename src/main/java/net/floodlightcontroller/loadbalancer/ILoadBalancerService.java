package net.floodlightcontroller.loadbalancer;

import java.util.Collection;

import net.floodlightcontroller.core.module.IFloodlightService;

public interface ILoadBalancerService extends IFloodlightService {

	//Get pool list. return all pools when pid=0
    public Collection<LBPool> getPools(String poolId);

    //Create new pool
    public int createPool(LBPool pool);
    
    //Update pool info
    public int updatePool(LBPool pool);

    //Delete pool
    public int deletePool(String poolId);

    //////////////////////
    //Get back server list. 
    public Collection<LBBkServers> getBkServers(String bksId);
    
    //Create new back server
    public int createBkServer(LBBkServers bks);
    
    //Update back server
    public int updateBkServer(LBBkServers bks);
    
    //Delete back server
    public int deleteBkServer(String bksId);
    
	//////////////////////////////////////////////
	//Create new vlan
	public int createVlan(LBVlan vlan);
	
	//Update vlan info
	public int updateVlan(LBVlan vlan);
	
	//Delete vlan
	public int deleteVlan(String vlanId);
	
	//Get back vlan list. 
    public Collection<LBVlan> getVlans(String vlanId);

    //////////
    //add back server to pool
    public int addBkstoPool(String poolId, String bksId);
    
    //delete back server from pool
    public int delBksfromPool(String poolId, String bksId);
    
    //add back server to vlan
    public int addBkstovlan(String vlanId, String bksId);
    
    //delete back server from vlan
    public int delBksfromVlan(String vlanId, String bksId);
    
    public int routeAddr(String dpid);
    
    
    


}

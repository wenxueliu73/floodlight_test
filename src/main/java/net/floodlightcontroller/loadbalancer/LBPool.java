package net.floodlightcontroller.loadbalancer;

import java.util.ArrayList;
import java.util.HashMap;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import net.floodlightcontroller.util.MACAddress;

@JsonSerialize(using=LBPoolSerializer.class)
public class LBPool {

	private String id;//pool id
	private int pos;//Position for round robin
	private String name; // Pool name
    private String vip; // virtual ip of the pool
    private MACAddress vipMac;//coped from default module
    public static String LB_PROXY_MAC= "12:34:56:78:";//coped from default module
	private int lbMethod = 0; // LB method, only 0 is supported in this version
    private int isactive; // Pool status, 1 active, 0 not acitve.
    private int protocol; // all 0 tcp 1 udp 2
    private short port;
    
    private ArrayList<LBBkServers> servers = new ArrayList<LBBkServers>(); // back
                                                                            // servers
                                                                            // in
                                                                            // the
                                                                            // pool

  
    
    private ArrayList<String> servermapkey = new ArrayList<String>();
    private ArrayList<String> servermapkeyplus = new ArrayList<String>();

	private HashMap<String, LBBkServers> serversmap = new HashMap<String, LBBkServers>(); // back
                                                                                            // servers
                                                                                            // in
                                                                                            // the
                                                                                            // pool
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}


	public ArrayList<String> getServermapkeyplus() {
		return servermapkeyplus;
	}

	public void setServermapkeyplus(ArrayList<String> servermapkeyplus) {
		this.servermapkeyplus = servermapkeyplus;
	}

	public ArrayList<String> getServermapkey() {
		return servermapkey;
	}

	public void setServermapkey(ArrayList<String> servermapkey) {
		this.servermapkey = servermapkey;
	}

	public HashMap<String, LBBkServers> getServersmap() {
		return serversmap;
	}

	public void setServersmap(HashMap<String, LBBkServers> serversmap) {
		this.serversmap = serversmap;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVip() {
		return vip;
	}

	public void setVip(String vip) {
		this.vip = vip;
	}

	public int getLbMethod() {
		return lbMethod;
	}

	public void setLbMethod(int lbMethod) {
		this.lbMethod = lbMethod;
	}

	public int getIsactive() {
		return isactive;
	}

	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}

	public int getProtocol() {
		return protocol;
	}

	public void setProtocol(int protocol) {
		this.protocol = protocol;
	}

	public short getPort() {
		return port;
	}

	public void setPort(short port) {
		this.port = port;
	}

	public ArrayList<LBBkServers> getServers() {
		return servers;
	}
	 public void addBkServer(LBBkServers bs) {
	        this.servers.add(bs);
	       // this.serversmap.put(bs.getIp()+":"+bs.getPort(), bs);
	        if(bs.getIsactive()==1)
	        {
	        	this.serversmap.put(bs.getIp()+":"+bs.getPort(), bs);
	        	this.servermapkey.add(bs.getIp()+":"+bs.getPort());
	        	for(int i=0;i<bs.getPriority();i++)
	        		this.servermapkeyplus.add(bs.getIp()+":"+bs.getPort());
	        }
	 }

	 public boolean deleteBkServer(String id) {
	        boolean r = true;
	        int i = 0;
	        LBBkServers bs=null;
	        //delete from back server list
	        for (LBBkServers bk : this.servers) {
	            if (id.equals(bk.getId())) {
	            	bs=bk;
	                this.servers.remove(i);
	                r = true;
	                break;
	            }
	            i = i + 1;
	        }
	       if(bs==null)
	    	   return r;
	        //delete from available server list
	        this.serversmap.remove(bs.getIp()+":"+bs.getPort());
	        //delete from server key list
	        this.servermapkey.remove(bs.getIp()+":"+bs.getPort());
	        while(this.servermapkeyplus.remove(bs.getIp()+":"+bs.getPort()))
	        {
	        	
	        }
	        return r;
	}
	 
	 public boolean updateBkServer(LBBkServers bs) {
	        boolean r = true;
	        int i = 0;
	        String id=bs.getId();
	        LBBkServers oldbk=null;
	        //update back server list
	        for (LBBkServers bk : this.servers) {
	            if (id.equals(bk.getId())) {
	            	oldbk=bk;
	                this.servers.remove(i);
	                this.servers.add(bs);
	                r = true;
	                break;
	            }
	            i = i + 1;
	        }
	        if(oldbk==null)
	        	return false;
	        //update available server list
	        this.serversmap.remove(oldbk.getIp()+":"+oldbk.getPort());
	        while(this.servermapkeyplus.remove(bs.getIp()+":"+bs.getPort()))
	        {
	        	
	        }
	        //only add to available server list if the server is active
	        if(bs.getIsactive()==1) 
	        	this.serversmap.put(bs.getIp()+":"+bs.getPort(), bs);
	        //update server key list
	        this.servermapkey.remove(oldbk.getIp()+":"+oldbk.getPort());
	        if(bs.getIsactive()==1) 
	        {
	        	this.servermapkey.add(bs.getIp()+":"+bs.getPort());
	        	for(int j=0;j<bs.getPriority();j++)
	        		this.servermapkeyplus.add(bs.getIp()+":"+bs.getPort());
	        }
	        return r;
	}
	 
	public void setServers(ArrayList<LBBkServers> servers) {
		this.servers = servers;
	}

	/*public HashMap<String, LBBkServers> getServersmap() {
		return serversmap;
	}

	public void setServersmap(HashMap<String, LBBkServers> serversmap) {
		this.serversmap = serversmap;
	}
*/
	
    public MACAddress getVipMac() {
		return vipMac;
	}

	public void setVipMac(MACAddress proxyMac) {
		this.vipMac = proxyMac;
	}
	

    public LBPool(String name, String vip, int lm, int protocol, short port,
            int isactive) {
    	String tmac="";
    	this.id=name;
        this.name = name;
        this.vip = vip;
        this.lbMethod = lm;
        this.protocol = protocol;
        this.port = port;
        this.isactive = isactive;
        tmac=leftPad(this.id,"0",4);
        this.vipMac = MACAddress.valueOf(LB_PROXY_MAC+tmac.substring(0, 2)+":"+tmac.substring(2, 4));
    }
    public LBPool() {
    	String tmac="";
    	this.id=String.valueOf((int) (Math.random()*10000));
        this.name = "";
        this.vip = "";
        this.lbMethod = 1;
        this.protocol = 1;
        this.port = 1;
        this.isactive = 1;
        tmac=leftPad(this.id,"0",4);
        this.vipMac = MACAddress.valueOf(LB_PROXY_MAC+tmac.substring(0, 2)+":"+tmac.substring(2, 4));
    }
    
    public static String leftPad(String str,String pad,int len){
    	  String newStr=(str==null?"":str);
    	  while(newStr.length()<len){
    	   newStr=pad+newStr;
    	  }
    	  if(newStr.length()>len){
    	   newStr=newStr.substring(newStr.length()-len);
    	  }
    	  return newStr;
    }

}

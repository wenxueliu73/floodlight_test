package net.floodlightcontroller.loadbalancer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@JsonSerialize(using=LBBkServersSerializer.class)
public class LBBkServers{
	
	private String id; // RandomID
	private String name; // Server name
    private String ip; // Server IP
    private short port;//Server Port
    private int priority;//priority
    private int isactive; // Pool status, 1 active, 0 unactive
    private String vlanid;//vlan id.
    private String poolid;//pool id

    public LBBkServers(String name, String ip, short port, int priority,int isactive,String vlanid,String poolid) {
    	this.id=name;
        this.name = name;
        this.ip = ip;
        this.port=port;
        this.priority=priority;
        this.isactive = isactive;
        this.vlanid=vlanid;
        this.poolid=poolid;
    }
    
    public LBBkServers() {
    	this.id="";
        this.name = "";
        this.ip = "";
        this.port=1;
        this.priority=1;
        this.isactive = 1;
        this.vlanid=null;
        this.poolid=null;
    }


    public String getVlanid() {
		return vlanid;
	}

	public String getPoolid() {
		return poolid;
	}

	public void setPoolid(String poolid) {
		this.poolid = poolid;
	}

	public void setVlanid(String vlanid) {
		this.vlanid = vlanid;
	}

	@Override
	public String toString() {
		return "LBBkServers [id=" + id + ", name=" + name + ", ip=" + ip
				+ ", port=" + port + ", priority=" + priority + ", isactive="
				+ isactive + "]";
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}


	public short getPort() {
		return port;
	}


	public void setPort(short port) {
		this.port = port;
	}


	public int getPriority() {
		return priority;
	}


	public void setPriority(int priority) {
		this.priority = priority;
	}


	public int getIsactive() {
		return isactive;
	}


	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		result = prime * result + port;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LBBkServers other = (LBBkServers) obj;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (port != other.port)
			return false;
		if (vlanid != other.vlanid)
			return false;
		return true;
	}
}

###编译

$ ant 

###物理环境部署

v350 两个端口连接到两台服务器, 其中一台为客户端 ip 10.1.2.25, 另一台是服务器 ip 10.1.2.11 并且运行 nginx (配置文件修改服务端口 8000).

注: v350 工作在 nat 模式 openflow10 协议.

###运行控制器

sudo ./floodlight.sh

###在交换机创建默认流表

   ovs-ofctl add-flow bro "priority=0,acitons=controller"

###客户端测试 

curl http://10.1.2.11:8000/  

正常返回

curl http://10.1.2.100:8000/

返回页面与上相同. 即负载均衡可以工作. 

###安装 ab

yum install apache2-util (apt-get install apache2-util).

###开始性能测试

ab -r -c 10 -n 1000 http://10.1.2.100:8000/

-c : 并发数
-n : 总请求数